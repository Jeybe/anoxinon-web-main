---
date: "2021-12-10T20:15:00+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter November 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den November 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/12/the-xmpp-newsletter-november-2021/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat November 2021 - dem letzten Newsletter für dieses Jahr! Nach unserer redaktionellen Pause werden wir im Februar 2021 zurück sein!

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:

- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news.jabberfr.org/category/newsletter/)
- Die italienische Version des letzten Monats auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)

Vielen dank an die Übersetzer:innen und deren Arbeit! Das ist eine klasse Hilfe, um die Neuigkeiten vielen zugänglich zu machen! Hilf auch du ihnen bei ihrer Arbeit oder ergänze eine weitrere Sprache!

## XSF Ankündigungen

- Die XSF-Mitglieder haben ihr neues [XSF-Board und XSF-Council [EN]](https://xmpp.org/about/xmpp-standards-foundation/#council) gewählt! Herzlichen Glückwunsch an alle!
- Die XSF bietet jetzt [fiscal hosting [EN]](https://xmpp.org/community/fiscalhost/) für XMPP-Projekte an! Bitte bewirb dich über [Open Collective [EN]](https://opencollective.com/xmpp). Weitere Informationen findest du im [Ankündigungs-Blogpost](https://xmpp.org/2021/09/the-xsf-as-a-fiscal-host/).
	- Außerdem hat die XSF ihr erstes Projekt in das Fiscal-Hosting-Programm aufgenommen! Ein herzliches Willkommen an das [MAM Plugin für XMPP.js [EN]](https://gitlab.com/deblounge/xmppjs-mam-plugin) (das Plugin ist unabhängig vom Upstream-Projekt [xmpp.js](https://github.com/xmppjs/xmpp.js))! Die Entwickler werden daran arbeiten, MAM-Unterstützung zu xmpp.js hinzuzufügen, mit dem letztendlichen Ziel, es der Matrix Bifrost-Brücke hinzuzufügen, so dass XMPP-Benutzer:innen den Verlauf ihrer bevorzugten Matrix-Kanäle abrufen können. Du kannst [hier für die Bemühungen spenden [EN]](https://opencollective.com/bifrost-mam).
- Die XSF plant die Teilnahme am Google Summer of Code 2022 (GSoC). Wenn du daran interessiert bist, als Student, Mentor oder als Projekt im Allgemeinen teilzunehmen, füge bitte [deine Ideen hinzu und nimm Kontakt mit uns auf [EN]](https://wiki.xmpp.org/web/Google_Summer_of_Code_2022)!
- Die Blog- und Newsletter-Seiten auf [xmpp.org/blog](https://xmpp.org/blog) unterstützen jetzt mehrere Sprachen. Wir freuen uns über Freiwillige, die beim Übersetzen helfen!

## Veranstaltungen

[XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Besuche auch unseren neuen [YouTube Channel [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)!

[Berlin XMPP Meetup (remote) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Monatliches Treffen von XMPP-Enthusiasten in Berlin - immer am 2. Mittwoch des Monats.

## Videos

Sam Whited hielt einen [Vortrag für die XMPP Office Hours [EN]](https://www.youtube.com/watch?v=lprIwxyPY2E) über den neuen Fiskal-Hosting-Service der XSF!

Gastvortrag beim Berliner XMPP Meetup: [Diving deep into Briar at the XMPP Meetup Berlin [EN]](https://nico.dorfbrunnen.eu/posts/2021/diving-at-xmpp/)

## Artikel

[Der PeerTube-Live-Chat verwendet XMPP [EN]](https://joinpeertube.org/news#live-plugin-app): Gib den Zuschauern deiner Instanz die Möglichkeit, während Live-Streams zu chatten!

XMPP anstelle von ActivityPub verwenden? Nun, [openEngiadina hat sich dafür entschieden [EN]](https://inqlab.net/2021-11-12-openengiadina-from-activitypub-to-xmpp.html)!

Im [September '21](/blog/xmpp_newsletter_september_2021/) Newsletter vergessen: [Nicholas A. Ferrell [EN]](https://emucafe.club/channel/naferrell) hat über ihren [Übergang zu XMPP für SMS-Kommunikation [EN]](https://thenewleafjournal.com/sending-sms-messages-from-my-computer-with-xmpp-through-jmp/) via [jmp.chat [EN]](https://jmp.chat/) geschrieben.

[Nicola Fabiano](https://www.nicfab.it/) ist mit einem weiteren Artikel zurück, in dem sie erklärt, warum sie sich für XMPP und speziell für das Hosting von [Snikket [EN]](https://snikket.org/) entschieden hat, um [Kontrolle über persönliche Informationen [EN]](https://www.nicfab.it/en/xmpp-is-a-solution-that-allows-users-to-have-control-over-their-personal-information-our-choice-snikket/) zu bekommen. Der Artikel ist auch auf [Italienisch](https://www.nicfab.it/xmpp-e-una-soluzione-che-permette-agli-utenti-di-avere-il-controllo-sulle-loro-informazioni-personali-la-nostra-scelta-snikket/) verfügbar.

[Take Back Our Tech [EN]](https://takebackourtech.org) hat eine Reihe von Artikeln (und dazugehörigen Video-Podcasts) über "XMPP: A Comeback Story" gestartet. Der erste heißt [A 20 Year Old Messaging Protocol For Robust, Private and Decentralized Communications [EN]](https://takebackourtech.org/xmpp-comeback/) und behandelt das "Ökosystem" von Anwendungen und Servern mit einer Schnellstartanleitung. Der zweite geht weiter mit [anonymen Anrufen und Textnachrichten mit JMP.Chat [EN]](https://takebackourtech.org/xmpp-jmp/).

Niklas von [gnulinux.ch](https://gnulinux.ch/) fragt "Verliert die Freie-Software-Gemeinschaft ihre Werte?" und fragt sich, wie es kommt, dass die Teilnahme an Diskussionen über freie Software nicht mehr möglich ist, ohne unfreie Software zu verwenden. Ließ [den vollständigen Artikel](https://gnulinux.ch/verliert-die-free-software-community-ihre-werte), um herauszufinden, warum dies wichtig ist und welche Lösung bereits existiert.

Im jmp.chat Blog gab es zwei kleine Updates, eine Anleitung [Wie man sich mit Movim anmeldet [EN]](https://blog.jmp.chat/b/subscribe-using-movim) und [ein Update [EN]](https://blog.jmp.chat/b/november-newsletter-2021) über die offiziellen Möglichkeiten, mit der Community zu kommunizieren und über die kommende App (mit Dialer-Integration). 

## Software News

### Clients und Apps

[Gajim News aus der Entwicklung](https://gajim.org/de/post/2021-11-29-development-news-november/): Genervt von Spam-Nachrichten in öffentlichen Kanälen? Gajim hat gerade Unterstützung für die Nachrichtenmoderation erhalten. Außerdem diesen Monat: bessere Nachrichtenkorrekturen und verbesserte Benachrichtigungen.

![Gajim Message Moderation](/img/blog/xmpp_newsletter_november_2021/gajim-message-moderation.png)

[xmpp-dns [EN]](https://salsa.debian.org/mdosch/xmpp-dns) wurde in der Version [`0.2.2`](https://salsa.debian.org/mdosch/xmpp-dns/-/releases/v0.2.2) veröffentlicht, ein kleines Update, das die Möglichkeit hinzufügte, auf das Testen von Standard-Ports zurückzugreifen, wenn keine XMPP SRV-Einträge vom Server bereitgestellt werden.

[UWPX [EN]](https://uwpx.org) wurde in Version [`0.38.0.0`](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.38.0.0) veröffentlicht. Diese Version aktualisiert die OMEMO-Implementierung von `0.7.0 (2020-09-05)` auf `0.8.1 (2021-10-07)` und enthält eine Reihe von OMEMO-bezogenen Fehlerbehebungen.

[SiskinIM [EN]](https://siskin.im/) Version [`7.0`](https://github.com/tigase/siskin-im/releases/tag/7.0) wurde veröffentlicht. Es handelt sich um eine größere Version mit Unterstützung für [XEP-0333 Chat Markers [EN]](https://xmpp.org/extensions/xep-0333.html), Standortfreigabe und verbesserter UI/UX, Abruf der Historie, Cache-Handling und Push-Verbesserungen neben einer Reihe von Bugfixes.

[BeagleIM [EN]](https://beagle.im/) Version [`5.0`](https://github.com/tigase/beagle-im/releases/tag/5.0) wurde veröffentlicht. Es handelt sich um eine größere Version mit Unterstützung für [XEP-0333 Chat Markers](https://xmpp.org/extensions/xep-0333.html), Bildschirmfreigabe, Sprachnachrichten und Standortfreigabe, verbesserte UI/UX, Abrufen der Historie und vieles mehr.

[Libervia (ehemals "Salut à Toi") `0.8` "La Cecília" wurde veröffentlicht [EN]](https://www.goffi.org/b/libervia-v0-8-la-cecilia-BdQ4). Libervia ist ein Multi-Frontends-Client mit sozialen Funktionen wie Blogging, Fotoalben, Veranstaltungsorganisation usw. Diese Veröffentlichung ist ein großer Meilenstein, um Libervia zu einem sozialen Netzwerk für Freunde und Familie zu machen.

Eine neue Hauptversion von [Converse.js [EN]](https://conversejs.org/) ist nach dreimonatiger Entwicklungszeit erschienen, [Version `9.0.0`](https://github.com/conversejs/converse.js/releases/tag/v9.0.0) bringt komprimierte Avatare, neue Einstellungen für die Medienwiedergabe und eine Menge Fehlerbehebungen. Admins sollten die Versionshinweise lesen und ihre Installationen aktualisieren.

Die neue Converse.js Version wurde auch in der [inVerse Plugin Version `9.0.0.1` [EN]](https://discourse.igniterealtime.org/t/inverse-plugin-for-openfire-version-9-0-0-1-released/91075) für den [Openfire Server [EN]](https://www.igniterealtime.org/projects/openfire/) aktualisiert und sollte in der Liste der Instanz-Updates erscheinen.

### Server

Das neue [Snikket-Server-Update [EN]](https://snikket.org/blog/nov-2021-server-release/) bringt Verbesserungen für die Benutzer:inneninteraktion auf iOS, ein erhöhtes Limit für die Dateifreigabe, Ressourcenüberwachung und eine bessere Kontoverwaltung!

[MongooseIM 5.0 [EN]](https://github.com/esl/MongooseIM/releases/tag/5.0.0) wurde am 7. Oktober veröffentlicht! Was ist neu? Dynamische XMPP-Domains, verbesserte Dokumentation, mehrere Fehlerbehebungen und mehr. Das virtuelle Hosten von XMPP-Domains war bereits mit MongooseIM möglich, aber dynamische Domains machen es möglich, neue Domains hinzuzufügen, ohne den Server neu zu starten - und das in großer Zahl! Lasttests mit bis zu 100k Benutzer:innen haben gezeigt, dass es praktisch keinen Unterschied mehr macht, ob sich alle Benutzer mit einer einzigen Domain oder mit 100k Domains verbinden - also eine Domain pro Benutzer:in - ja, so flexibel ist es!

### Libraries (Bibliotheken)

Das Mellium [Dev Communiqué für November 2021 [EN]](https://opencollective.com/mellium/updates/dev-communique-for-november-2021) wurde veröffentlicht! Diesen Monat konzentrierte sich die Arbeit hauptsächlich auf das [carbons package [EN]](https://pkg.go.dev/mellium.im/xmpp/carbons) und auf die Erstellung einer wiederverwendbaren Test-Suite, die andere Bibliotheken importieren können, um [message styling [EN]](https://pkg.go.dev/mellium.im/xmpp/styling) zu testen.

Openfire Smack veröffentlicht [Version `4.4.4` als Patch Level Release [EN]](https://discourse.igniterealtime.org/t/smack-4-4-4-released/90954)!

## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

* Diesen Monat werden keine XEPs vorgeschlagen.

### New (Neu)

* Diesen Monat gibt es keine neuen XEPs.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

* Diesen Monat wurden keine XEPs zurückgestellt.

### Updated (Aktualisiert)

- Version 0.2.0 von [XEP-0459 [EN]](https://xmpp.org/extensions/xep-0459.html) (XMPP Compliance Suites 2022)
    - Umbenennung von Advanced Server und Advanced Client in Server und Client
    Hinzufügen von XEP-0455 zum Abschnitt "Zukünftige Entwicklung" (sp)


### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

* Keine Last Calls diesen Monat.


### Stable (Stabil)
Info: Die XSF hat beschlossen, "Draft" in "Stable" umzubenennen. [Ließ mehr darüber hier [EN]](https://github.com/xsf/xeps/pull/1100).

- Version 1.0.0 von [XEP-0459 [EN]](https://xmpp.org/extensions/xep-0459.html) (XMPP Compliance Suites 2022)
    - Vorrücken zum Entwurf gemäß Ratsvotum vom 2021-11-03. (XEP Editor (jsc))
- Version 1.0.0 von [XEP-0313 [EN]](https://xmpp.org/extensions/xep-0313.html) (Verwaltung von Nachrichtenarchiven)
    - Vorrücken zu Stable gemäß Ratsbeschluss vom 2021-10-27. (XEP-Redakteur (jsc))

### Deprecated (Veraltet)

* Keine veralteten XEPs diesen Monat.


### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

* Kein Call for Experience in diesem Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an xdelatour, wurstsalat3000, seveso, palm123, Nicola Fabiano, mdosch, MattJ, Licaon_Kter, Goffi, erszcz, emus, Benoît Sibaud, Anoxinon e.V., Adrien Bourmault (neox) ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook [EN]](https://www.facebook.com/jabber/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

