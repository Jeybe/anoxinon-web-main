--- 
date: "2021-10-10T19:31:59+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter September 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den September 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/10/the-xmpp-newsletter-september-2021/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat September 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:

- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, das Newsletter-Team zu unterstützen? Ließ mehr am Ende.
Abseits davon - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht werden (mit etwas Verzögerung):

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [jabberfr.org](https://news.jabberfr.org/category/newsletter/) und [linuxfr.org](https://linuxfr.org/tags/xmpp/public)
- Die italienische Version  auf [NicFab.it](https://www.nicfab.it/)

Vielen Dank an all die Übersetzer:innen und deren Arbeit! Das ist eine große Hilfe, unsere News zu verbreiten! Bitte helft ihnen bei deren Arbeit oder bereichere uns um eine neue Sprache!

## XSF Ankündigungen

Die XSF kann nun als [steuerlicher/"fiscal"-Host [EN]](https://xmpp.org/community/fiscalhost/) für andere XMPP-Projekte dienen. Bitte via OpenCollective bewerben. Für mehr Informationen, sieh' dir den [Ankündigungs-Blogartikel [EN]](https://xmpp.org/2021/09/the-xsf-as-a-fiscal-host/) an.

Die XSF plant am Google Summer of Code 2022 (GSoC) teilzunehmen. Wenn du daran interessiert bist als Stundent, Mentor oder mit einem Projekt generell mitzumachen, dann [teile doch bitte deine Ideen und trete mit uns in Kontakt [EN]](https://wiki.xmpp.org/web/Google_Summer_of_Code_2022).

Außerdem hat die [xmpp.org-Webseite [EN]](https://xmpp.org/) eine Aktualisierung bekommen. Sie wird nun mit [Hugo [EN]](https://gohugo.io/) gebaut (statt Pelican), was den Wartungsaufwand signifikat verringert. Die neue Webseite basiert auf Bootstrap 5 und wurde unter dem Gedanken des Minimalismus entwickelt. Wir haben uns ebenfalls bemüht, Beiträge dazu so einfach wie möglich zu gestalten. Das lokale Bauen der Webseite hat nur minimale Abhängigkeiten und kann wahlweise auch mit Docker und Vagrant geschehen.

## Veranstaltungen

[XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Siehe dazu auch unseren neuen [YouTube-Kanal [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)!

[Berlin XMPP Meetup (virtuell) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Monatliches Meeting von XMPP-Enthusiast:innen in Berlin - immer an jedem zweiten Mittwoch eines jeden Monats.

## Artikel

OpenPGP for XMPP (OX) schafft es langsam in Client-Implementierungen. In einem Deutschen Blogbeitrag beschreibt DebXWoody Schritt für Schritt, wie man [OX in Profanity aktivieren und verwenden kann [EN]](https://mov.im/?blog/debxwoody%40movim.eu/profanity-und-openpgp-for-xmpp-ox-YRMREh). 

Die Arbeit am Libervia ActivityPub Gateway schreitet voran, mit [einem Bericht [EN]](https://www.goffi.org/b/libervia-progress-note-2021-w38--flt) über Volltextsuche für den PubSub-Cache und einer frühen, aber funktionierenden ActivityPub XMPP-Komponente.

![Bildschirmfoto zweier Mastodon-Beiträge: "This is an AP message posted with Mastodon" und "This is an XMPP message posted with libervia](/img/blog/xmpp_newsletter_september_2021/libervia.jpg)

Matthew Wild hat ein Web-Werkzeug zum Erkunden von [XEP-0392 "Consistent Color Generation" [EN]](https://xmpp.org/extensions/xep-0392.html) veröffentlicht. Dieses XEP rät Clients, wie sie die Kontakte eines/einer Nutzer:in einfärben soll (Bspw. deren Nicknamen oder Standardavatare), zum Zwecke der einfacheren visuellen Identifizierung. Das XEP beschreibt einen Standardalgorithmus, welcher darauf abzielt eine markante Farbe für jeden Kontakt vorzugeben und bei allen Clients gleich anzuzeigen, unter Berücksichtigung von Farbsehschwächen. Siehe auch [XEP-0392 Coleur Explorer [EN]](https://matthewwild.co.uk/projects/xep0392-web/) und [Modern XMPP Colour Guidance [EN]](https://docs.modernxmpp.org/client/design/#auto-generated-colors).

Schonmal einen Vergleich zwischen Webclients von XMPP und Matrix gesucht? Dann hast du nun Glück, denn [Ade Malsasa Akbar [EN]](https://floss.social/@ademalsasa) hat [eine kurze Übersicht [EN]](https://www.ubuntubuzz.com/2021/09/element-and-movim-messengers-comparison-made-simple.html) über zwei Gruppenchat-Messenger der dezentralen Gattung geschrieben, Element von Matrix und Movim von XMPP.

## Software News

### Clients und Apps

[Dino v0.2.2 [EN]](https://github.com/dino/dino/releases/tag/v0.2.2) wurde veröffentlicht. Diese Version ist eine Wartungsveröffentlichung und beinhaltet Fehlerbehebungen.

UWPX [V.0.35.1.0 [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.35.1.0) und [V.0.36.0.0 [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.36.0.0) wurden veröffentlicht. [V.0.35.1.0 [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.35.1.0) bringt endlich Push-Unterstützung für den [Push Server [EN]](https://github.com/UWPX/UWPX-Push-Server) von [COM8 [EN]](https://github.com/COM8). [V0.36.0.0 [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.36.0.0) von UWPX behebt einige Fehler und hebt die UI auf [WinUI 2.7 [EN]](https://docs.microsoft.com/en-us/windows/apps/winui/winui2/release-notes/winui-2.7). Abseits davon gibt es nun einen [OMEMO-Status-Indikator [EN]](https://user-images.githubusercontent.com/11741404/135456998-5a0c9e0a-181c-46ab-ae94-3dff8e3b947e.png), mit welchem du prüfen kannst, ob deine Kontakte den aktuellsten OMEMO-Standard unterstützen.

[XMPP-DNS [EN]](https://salsa.debian.org/mdosch/xmpp-dns), ein Werkzeug, um XMPP-SRV-Records abzurufen und um die Verbindung zu testen, ist in einer initalen Veöffentlichung in [V0.1.0 [EN]](https://salsa.debian.org/mdosch/xmpp-dns/-/releases/v0.1.0) erschienen. Auf die Veröffentlichung folge direkt [V0.2.0 [EN]](https://salsa.debian.org/mdosch/xmpp-dns/-/releases/v0.2.0), die Untersützung für XMPP-Server-SRV-Records hinzufügt und eine [V0.2.1 [EN]](https://salsa.debian.org/mdosch/xmpp-dns/-/releases/v0.2.1) mit Fehlerbehebungen. 

[Gajim News aus der Entwicklung [DE]](https://gajim.org/de/post/2021-09-29-development-news-september/): Der September hat viele Neuerungen unter der Haube gebracht. In Vorbereitung auf Gajim 1.4, welches große Veränderungen bringt, wurden viele Teile des Quelltextes angepasst. Diese Änderungen sind für Nutzer:innen großteils unsichtbar, machen Gajim aber robuster. In einigen Fällen hat dies auch visuelle Verbesserungen zur Folge: Die beiden Fenster zum Hinzufügen eines Kontakts und Starten eines Chats erkennen nun den Typ des Chats hinter einer Adresse.

[Go-sendxmpp [EN]](https://salsa.debian.org/mdosch/go-sendxmpp), eine der vielen [Alternativen [EN]](https://wiki.xmpp.org/web/User:MDosch/Sendxmpp_incarnations) zum originalen [sendxmpp [EN]](https://sendxmpp.hostname.sk/) veröffentlichte die Versionen [V0.1.0 [EN]](https://salsa.debian.org/mdosch/go-sendxmpp/-/releases/v0.1.0) und [V0.1.1 [EN]](https://salsa.debian.org/mdosch/go-sendxmpp/-/releases/v0.1.1).

Conversations und Quicksy haben diesen Monat Version [2.10.0 [EN]](https://github.com/iNPUTmice/Conversations/releases/tag/2.10.0) mit einem kurzen Changelog herausgebracht: schwarze Balken bei Videoanrufen (damit Sie wissen, wann Sie "falsch halten"), Verbesserungen bei der Suchleistung und eine neue Einstellung zum Blockieren von App-Screenshots. Unter der Haube gab es noch mehr: zwei Fehler bei Dateianhängen wurden behoben (speziell für Nutzer mit vielen Mediendateien), durch Berühren der Titelleiste werden Chat-Details geöffnet und verschachtelte Zitate (noch nicht standardmäßig, aber du kannst "Kopieren" und dann "Als Zitat einfügen" verwenden).

Converse wird nach langer Entwicklungszeit weiter voran gebracht. [Version 8 dieses JavaScript-XMPP-Chat-Clients, der in deinem Browser läuft [EN]](https://opkode.com/blog/2021-09-13-converse-8/), wurde veröffentlicht. Der Blogbeitrag von [JC Brand [EN]](https://opkode.com/) behandelt die sichtbaren Änderungen (Nachrichtengestaltung, OMEMO-verschlüsselte Dateien, URL-Vorschau), aber auch die internen Änderungen (IndexDB als Standard, Webkomponenten). [8.0.1 [EN]](https://github.com/conversejs/converse.js/releases/tag/v8.0.1) folgte kurz darauf mit Fehlerbehebungen für das ausgefeilte Produkt.
![Bildschirmfoto Converse 8.0.1](/img/blog/xmpp_newsletter_september_2021/converse-fullscreen.png)

Profanity [0.11.1 [EN]](https://github.com/profanity-im/profanity/releases/tag/0.11.1) wurde veröffentlicht und verbessert Themes, Benachrichtigungen und die Handhabung von OMEMO.

Das Mellium Dev Communiqué für September wurde veröffentlicht. Es enthält kleinere Aktualisierungen des [TUI-Clients von Communiqué [EN]](https://mellium.im/communique/) sowie der Bibliothek [mellium.im/xmpp [EN]](https://pkg.go.dev/mellium.im/xmpp). Alle Details im [Dev Communiqué für September 2021 [EN]](https://opencollective.com/mellium/updates/dev-communique-for-september-2021) auf der [Open Collective [EN]](https://opencollective.com/mellium) Seite.

### Server

* Diesen Monat haben uns keine Neuigkeiten über XMPP-Server erreicht :-(

### Libraries (Bibliotheken)

Mellium hat die Version 0.20.0 ihrer Go XMPP-Bibliothek veröffentlicht. Die Release-Ankündigung kann auf [Open Collective [EN]](https://opencollective.com/mellium/updates/release-mellium-im-xmpp) gefunden werden. Einige der größeren Funktionen sind Gruppen-Chat (MUC), Chat-Verlauf (MAM) und Unterstützung für Ad-hoc-Befehle!

## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

* Diesen Monat werden keine XEP vorgeschlagen.

### New (Neu)

* Diesen Monat gibt es keine neuen XEPs.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

* Diesen Monat wurden keine XEPs zurückgestellt.

### Updated (Aktualisiert)

- Version 0.8.0 von [XEP-0384 [EN]](https://xmpp.org/extensions/xep-0384.html) (OMEMO-Verschlüsselung)
    - Aktualisierung auf XEP-0420 Version 0.4.0 und Anpassung des Namensraumes
    - Ersetzen des alten "content"-Elements von SCE durch das neue "envelope"-Element
    - Ersetzen des alten SCE-Elements "payload" durch das neue Element "content".
    - Aktualisierung des SCE-Namensraums auf 'urn:xmpp:sce:1'.
    - Aktualisierung des Namensraums auf 'urn:xmpp:omemo:2' (melvo)
- Version 0.14.0 von [XEP-0280 [EN]](https://xmpp.org/extensions/xep-0280.html) (Message Carbons)
    - Einarbeitung von LC-Feedback: Entfernung der Anforderung, "private" Elemente zu entfernen (und Hinzufügen eines Interop-Hinweises), vollständige Umformulierung der Überlegungen zu mobilen Geräten, um der modernen Realität gerecht zu werden. (gl)
- Version 1.1 von [XEP-0227 [EN}]](https://xmpp.org/extensions/xep-0227.html) (Portable Import/Export Format für XMPP-IM Server)
    - Abraten von der Verwendung von "Passwort", Möglichkeit zur Einbeziehung von SCRAM-Anmeldeinformationen, PEP-Knoten und Nachrichtenarchiven. (mw)
- Version 1.22.0 von [XEP-0060 [EN]](https://xmpp.org/extensions/xep-0060.html) (Publish-Subscribe)
    - Entfernen der Ausnahme für das letzte Element beim Löschen eines Knotens: alle Elemente müssen entfernt werden. (jp)


### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

* [XEP-0459 [EN]](https://xmpp.org/extensions/xep-0459.html) XMPP Compliance Suites 2022

### Stable (Stabil (früher bekannt als Draft))

Info: Die XSF hat beschlossen, "Draft" in "Stable" umzubenennen. [Lies hier mehr darüber. [EN]](https://github.com/xsf/xeps/pull/1100)

* Kein Stable diesen Monat.

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

* Kein Call for Experience in diesem Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an Adrien Bourmault (neox), Benoît Sibaud, emus, Jeybe, jk-forensics, Licaon_Kter, MattJ, mdosch, nicola, palm123, seveso, Sam Whited, SouL, wurstsalat3000, Ysabeau for their support and help in creation, review and translation ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook [EN]](https://www.facebook.com/jabber/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

