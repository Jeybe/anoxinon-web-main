+++
date = "2018-12-19T18:00:00+01:00"
draft = false
author = "Anxi"
title = "Anoxinon Inside - Teil 1"
description = "Interview mit Skyfar aka. Christopher Bodtke"
categories = ["Allgemeines", "Verein", "Interview"]
tags = ["Verein","Interview","anoxinon", "gründung"]

+++
Nachdem es größeres Interesse  an den Personen hinter dem Verein gab, haben wir beschlossen eine kleine „Interviewreihe“ zu veröffentlichen.
Mein Name ist Anxi, ich bin das Maskottchen von Anoxinon und führe durch das Interview. :)

Mein heutiger Gast ist Christopher Bodtke, 1. Vorsitzender von Anoxinon.

---

**Anxi: Hallo Christopher, stell Dich doch mal unserer Community vor!**

*Christopher:* Hallo und Dankeschön für die Einladung erst mal, ich bin Christopher Sven Bodtke, 26 Jahre jung, geborener Berliner, 1. Vorsitzender sowie Systemadministrator des Vereins und ehemaliger Profi E-Sportler.

**Anxi: Womit beschäftigst Du Dich in deiner Freizeit? Was macht Dir am meisten Spaß?**

*Christopher:* Überwiegend beschäftige ich mich damit mein Wissen im IT – Feld zu erweitern, gerne lese ich auch Bücher aus dem Sci–Fi Genre, bevorzugt welche aus dem Metro 2033 Universum, oder schaue Filme & Serien. Meine Lieblingsserie ist seit je her Stargate SG-1. Ein nicht allzu kleiner Teil meiner Zeit geht aber vor allem für meine Familie, Freunde und Nachbarn drauf, mit denen Ich gerne abends zusammen sitze bei einem Bierchen.

**Anxi: Welches Betriebssystem verwendest Du und wieso?**

Christopher: Aktuell habe ich Windows (noch) und Linux (Fedora 28) im Einsatz. Windows ist und war nicht nur das Betriebssystem mit dem ich aufwuchs, sondern auch essentiell für meine Gaming Karriere gewesen. Aktuell rennt es nur noch auf einem Gerät von mir dem „Microsoft Surface Pro 4“ welches lediglich aus zeitlichen Gründen noch nicht zu einem Linux Convertible geworden ist. Wieso ich Linux als Desktop System verwende ist relativ simpel, es bietet mir die Möglichkeit einen Computer zu verwenden ohne das ich auf diesem verfolgt, ausspioniert oder auch eingeschränkt werde von großen Kommerziellen Unternehmen. Ich liebe die Freiheit die mir Linux anbietet, es so zu benutzen zu können wie ich es gerne möchte.

**Anxi: Was hat dein Interesse an Linux geweckt? Wie waren deine ersten Erfahrungen in diesem Bereich?**

*Christopher:* Mit Linux kam ich das erste Mal in meiner Schulzeit in Verbindung. Damals aber eher als Betriebssystem für meine Gaming Server (Debian). Später hat mich dann die Idee der freien Software dahinter gereizt, sodass ich mit einer ehemaligen Diaspora Kollegin zusammen auf Debian gewechselt bin. Durch den Sprung ins kalte Wasser konnten wir Linux als Desktop System kennen lernen. Aufgrund unserer damaligen, doch sehr geringen Kenntnissen, überlebte Debian aber nicht lange auf unseren Rechnern. Wir lernten jedoch, beim regelmäßigen zerlegen unserer Installationen, einiges was später auch Einfluss auf meine Server haben sollte. Nachdem ich dann meine ersten Gehversuche im Linux Alltag abbrach, brauchte es noch einige Jahre bis ich es schaffte komplett mit Linux als Desktop warm zu werden.

**Anxi: Wie ist deine Einstellung zu Dezentralen Netzwerken, sowie Open Source/Free Software? Gibt es etwas dass dich besonders fasziniert?**

*Christopher:* Für mich sind dezentrale Netzwerke ein wichtiger Bestandteil der heutigen Kommunikation. Nur durch diese wird es uns ermöglicht nicht von Unternehmen beim alltäglichen Miteinander im Internet ausspioniert und kommerzialisiert zu werden. Demzufolge stehe ich diesem also völlig offen gegenüber. Gerade solche Netzwerke die aus dem Open Source Bereich stammen und für ein neutraleres Netz sowie die bewusste Datenselbstbestimmung sorgen, sollten mehr gefördert werden. Generell kann man aber festhalten das die Open Source Kultur fördernswert ist, nur wenn der Quellcode für jedermann nachprüfbar ist, kann der Software auch am Ende vertraut werden.  

**Anxi: Gab es Ereignisse die maßgeblich zu dieser Einstellung beigetragen haben?**

*Christopher:* Ein besonderes maßgebliches Ereignis gab es eigentlich nie für mich. Mir war schon relativ früh klar, das große Technologie Firmen unsere Daten sammeln und diese verwenden. Als Jugendlicher interessierte mich das relativ wenig, der Fokus verlagerte sich erst mit der Zeit als mir bewusst wurde, dass ich für die Nutzung von bestimmten Diensten nicht „alle“ meine Daten Preis geben möchte. Natürlich, wie bei den meisten, haben auch bei mir die NSA/CIA/Snowden Leaks ihren Teil dazu beigetragen um diesen weg strikter zu verfolgen. Durch diesen ist mir aber erneut klar geworden, dass ich nicht nur für mich was verändern möchte sondern auch anderen dabei helfen muss ein besseres Bewusstsein für den Missbrauch von Daten zu bekommen.

**Anxi: Welche Themen bekommen zu wenig Aufmerksamkeit, deiner Meinung nach? Wo ist noch Nachholbedarf?**

*Christopher:* Es bekommen zu viele Themen zu wenig Aufmerksamkeit als das ich sie hier aufzählen könnte. Was mir aber immer wieder ein persönlicher Dorn im Auge ist, ist die dezentrale Community als solches. Gerade die ehrenamtlichen Administratoren die ihre Dienste für jedermann kostenfrei ins Netz stellen, bekommen viel zu wenig Support von ihren Nutzern. Ob das in schriftlicher oder finanzieller Form stattfindet spielt hier keine Rolle. Fakt für mich ist: Die Community muss enger zusammenwachsen und sich untereinander mehr fördern um das gemeinsame Ziel zu erreichen. „Unabhängigkeit von Konzernen“

**Anxi: Wie siehst du die Entwicklungen in der Zukunft?**

*Christopher:* Aktuell denke ich, dass wir eine echte Chance haben uns einen gewissen Grad von Unabhängigkeit zu erkämpfen. Dafür müssen wir zwar alle viel Leisten um weiter voran zu kommen, aber gerade die immer weiter voranschreitende Digitalisierung bietet uns viel mehr Möglichkeiten um Menschen mit unserer Message zu erreichen. Es wird sicher nicht einfach werden auf dem Weg zur Selbstbestimmung über unserer Daten, aber wenn wir nicht heute beginnen daran zu arbeiten werden wir in der Zukunft kein Erfolg ernten können. Im Angesicht der Bemühungen von vielerlei Leuten schaue ich also positiv auf die Zukunft.

**Anxi: Anderes Thema: Warum hast Du dich entschlossen, als einer der treibenden Kräfte, Anoxinon zu gründen?**

*Christopher:* Einer der wahrscheinlich offensichtlichen Gründe ist die Förderung des Datenschutzes in Deutschland. Es muss einfach wesentlich mehr Aufklärung in der Bevölkerung betrieben werden über dieses kritische Thema. Auf der anderen Seite war es mir aber auch ein Anliegen nicht nur aufzuklären, sondern auch praktische Mittel bereitzustellen die der Bürger verlässlich nutzen kann. All zu oft kommt es vor dass private Projekte auf Grund von finanziellen oder personellen Engpässen ihren Dienst einstellen müssen und so die Nutzer wieder auf „Straße“ sitzen. Um eine verlässliche Anlaufstelle anbieten zu können war mir also eine finanzielle, rechtliche und personelle Unabhängigkeit von „einer“ Person sehr wichtig. Daher der Weg aus dem privaten Projekt, hin zum Verein.

**Anxi: Worin siehst Du Deine Aufgabe im Verein? Was ist dein persönliches Ziel im und für den Verein?**

*Christopher:* Meine Aufgabe ist sehr klar definiert, ich bin nicht nur Gründer, 1. Vorsitzender und Mitglied des Vereins, sondern in meiner Kernaufgabe Technischer Leiter. Das heißt ich kümmere mich zwischen den Alltagsangelegenheiten des Vereins vor allem um die Weiterentwicklung, Administration, Planung und Umsetzung unserer Angebotenen Dienste im Zusammenspiel mit den Arbeitsgruppen. Mein persönliches Ziel im Verein ist es ihm ein guter 1. Vorsitzender zu sein und ihn in die richtigen Bahnen zu lenken, damit er im besten Fall wächst und fortlaufend zu einer tollen Institution heranreift. Mein Ziel für den Verein ist ganz klar, er möge eine Anlaufstelle werden für Hilfe suchende im Bereich des Datenschutzes und der Datensicherheit.

**Anxi:  Was hat Dich dazu bewegt als 1. Vorsitzender aktiv zu werden?**

*Christopher:* Die Frage ist wohl recht einfach beantwortet: Als einer der zwei Personen die die Ursprungsidee zu diesen Projekt hatten, ist es einfach mein persönliches Anliegen den Verein als eine der leitenden Mitglieder in die richtigen Bahnen zu lenken.

**Anxi: Danke für das Gespräch :)**

---

Das nächste Interview wird voraussichtlich kommenden Samstag erscheinen.  
Zu Gast ist nächstes Mal Thomas Leister, Vorstandsmitglied. Bis dahin :)
