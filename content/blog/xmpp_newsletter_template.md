--- 
date: "2050-01-01T23:59:59+01:00" 
draft: true
author: "Anoxinon" 
title: "XMPP Newsletter Template" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den MONAT JAHR" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/year/month/the-xmpp-newsletter-month-year/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP Newsletter, schön, dass du wieder da bist! Diese Ausgabe umfasst den MONAT JAHR.

Wie dieser Newsletter auch sind viele Projekte und deren Mühen in der XMPP Gemeinschaft das Ergebnis der Arbeit von Freiwilligen. Wenn du mit den von dir verwendeten Diensten und Applikationen zufrieden bist - gerade in der jetzigen Situation - dann denke bitte darüber nach, dich bei diesen Projekten zu bedanken oder helfend einzubringen! Bist du daran interessiert, das Newsletter-Team zu unterstützen? Ließ mehr am Ende des Artikels.

### Übersetzungen des Newsletters

Dies ist eine Gemeinschafstarbeit und wir wollen allen Übersetzer:innen für deren Beiträge danken! Freiwillige sind Willkommen! Die Übersetzungen des XMPP Newsletters werden auf den folgenden Seiten veröffentlicht (mit etwas Verzögerung):

- Französisch: [jabberfr.org](https://news.jabberfr.org/category/newsletter/) und [linuxfr.org](https://linuxfr.org/tags/xmpp/public)
- Deutsch: [xmpp.org/de/blog](https://xmpp.org/de/blog/) und [anoxinon.de](https://anoxinon.de/blog/)
- Italienisch: [nicfab.it](https://www.nicfab.it/)
- Spanisch: [xmpp.org/es/blog](https://xmpp.org/es/blog/)

## XSF Ankündigungen

## XSF fiscal hosting Projekte

## Veranstaltungen

## Artikel

## Software News

### Clients und Applikationen

## Server

### Libraries (Bibliotheken)

## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001](https://xmpp.org/extensions/xep-0001.html)  [EN] beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess)](https://xmpp.org/about/standards-process.html) [EN]. Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv](https://mail.jabber.org/pipermail/standards/) [EN].)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

### New (Neu)

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

### Updated (Aktualisiert)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

### Stable (Stabil)

Info: Die XSF hat beschlossen, "Draft" in "Stable" umzubenennen. [Lies mehr darüber hier](https://github.com/xsf/xeps/pull/1100) [EN].

### Deprecated (Veraltet)

### Obsoleted (Obsolet)

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon](https://fosstodon.org/@xmpp/) [EN]
* [YouTube](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) [EN]
* [Twitter](https://twitter.com/xmpp) [EN]
* [Reddit](https://www.reddit.com/r/xmpp/) [EN]
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/) [EN]

[Abonniere](https://tinyletter.com/xmpp) [EN] diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

Finde und schreibe XMPP Job-Angebote im [XMPP job board](https://xmpp.work/) [EN].

## Hilf uns, diesen Newsletter zu erstellen

Der XMPP-Newsletter wurde kollaborativ von der Community produziert. Danke an Adrien Bourmault (neox), alkino, anubis, Anoxinon e.V., Benoît Sibaud, cpm, daimonduff, emus, Ludovic Bocquet, Licaon_Kter, marevalo, mathieui, MattJ, nicfab, patasca, seveso, Sam Whited, singpolyma, TheCoffeMaker, wurstsalat,xdelatour und Ysabeau ihre Hilfe, ihn zu erstellen!

Jede monatliche Ausgabe des Newsletters wird in [einfachen Notiz](https://yopad.eu/p/xmpp-newsletter-365days) [EN] entworfen. Am Ende des Monats wird der Inhalt dieser Notiz in das [XSF Github Repositorium](https://github.com/xsf/xmpp.org/milestone/3) [EN] übertragen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat](xmpp:commteam@muc.xmpp.org?join) [EN] beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen. Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum sammeln
- Neuigkeiten und Veranstaltungen kurz zusammenfassen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz](https://creativecommons.org/licenses/by-sa/4.0/deed.de) [DE] veröffentlicht.