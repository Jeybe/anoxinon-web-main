+++ 
date = "2021-03-28T19:30:00+01:00" 
draft = false
author = "Jeybe, jonas-l" 
title = "XMPP Newsletter Feburar 2021" 
description = "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Februar 2021" 
categories = ["Community", "Inhalte", "Open Source"] 
tags = ["XMPP", "Messenger", "XSF", "XMPP Newsletter"]
+++ 

**Anmerkung:**
*[Anoxinon e.V.](https://anoxinon.de) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/02/newsletter-02-feburary/). Übersetzung und Korrektur der deutschen Version von jonas-l und Jeybe.*

## Vorwort

Willkommen zum XMPP-Newsletter für den Februar 2021.

Viele Projekte und deren Bestrebungen in der XMPP-Gemeinschaft sind ein Ergebnis der Arbeit von Freiwilligen. 
Wenn Du mit den von Dir genutzten Diensten und Anwendungen zufrieden bist, speziell auch im letzten Jahr, dann überlege Dir doch, danke zu sagen oder zu helfen.

[Ließ die englische Version des Newsletters als RSS-Feed](https://xmpp.org/feeds/all.atom.xml)!

Interessiert daran, das Newsletter-Team zu unterstützen? Am Ende gibt es mehr Infos! Davon abgesehen - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Übersetzungen des Newsletters werden hier erscheinen (mit etwas Verzögerung):

- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news.jabberfr.org/category/newsletter/).
- Die deutsche Übersetzung des [XMPP Newsletter für Januar 2021 gibt es bei uns auf Anoxinon e.V.](https://anoxinon.de/blog/xmpp_newsletter_januar_2021/).

## Veranstaltungen

[Berlin XMPP Treffen (virtuell)](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Das monatliche Meeting von XMPP-Enthusiasten in Berlin - immer am zweiten Mittwoch jedes Monats.

## Videos

Matthew Wild hat einen Vortrag über [Produkte vs Protokolle [EN]](https://peertube.thepacket.exchange/videos/watch/96e35d83-e70c-4a87-868b-f9cbeb48e1d0) auf der FOSDEM 2021 gehalten.

[XMPP - eine Alternative zu Telegram, WhatsApp & Co? Mit Daniel Gultsch und DIE LINKE Berlin](https://video.ploud.fr/videos/watch/7e6876ed-7d4b-478d-a850-25dc84dcaf96).

Der französische [april.org](https://april.org)  Gemeinschafts-Podcast _(oder on air on radio Cause Commune 93.1 FM und online)_ hatte am 26. Januar eine Sendung über freies Instant-Messaging mit Adrien Bourmault, Emmanuel Gil Peyrot und Nicolas Vérité. Wenn Du es nicht live gesehen hast, gibt es [die Aufnahme ab sofort [FR]](https://april.org/91-messageries-instantanees) _(interessant wird es ab 17:30)_. Eine Niederschrift [gibt es ebenso [FR]](https://www.librealire.org/emission-libre-a-vous-diffusee-mardi-26-janvier-2021-sur-radio-cause-commune#Les-messageries-instantanees-libres-avec-Adrien-Bourmault-Emmanuel-Gil-nbsp).

[Eine Schnelleinführung in XMPP (mit Node.js und eJabberd) [EN]](https://www.youtube.com/watch?v=OVN99SgBGkM) von Hussein Nasser. In diesem Video geht er die XMPP-Architektur durch, erklärt, wie sie funktioniert und zeigt schlussendlich, wie man einen XMPP-Chat-Server aufsetzt und zu diesem via Node.js. eine Verbindung aufbaut.

[Movim](https://movim.eu) wurde im [Destination Linux Podcast [EN]](https://youtu.be/NhQW2yETvwU?t=2833) erwähnt.

## Artikel

Snikket: [Produkte vs Protokolle: Was Signal richtig gemacht hat [EN]](https://snikket.org/blog/products-vs-protocols/). Diese erweiterte Version von Matthew Wilds [FOSDEM Vortrag [EN]](https://peertube.thepacket.exchange/videos/watch/96e35d83-e70c-4a87-868b-f9cbeb48e1d0) zeigt die Denkweise hinter Snikket auf und was XMPP und dezentralisierte Protokolle von Signals Erfolg lernen können.

Rohan Kumars Blogbeitrag über [WhatsApp und die Domestizierung von Nutzer:innen [EN]](https://seirdy.one/2021/01/27/whatsapp-and-the-domestication-of-users.html) ging im Internet umher und thematisiert XMPP und andere dezentrale Lösungen. Der Artikel wurde in mehrere Sprachen übersetzt und um eine Fortsetzung ["Plattformen offenhalten"](https://seirdy.one/2021/02/23/keeping-platforms-open.html) erweitert, die sich etwas mit der XMPP-Geschichte befasst.

Innerhalb des letzten Monates, speziell nach WhatsApps Ankündigung und nachfolgender Verschiebung der Aktualisierung derer Datenschutzbedingungen, haben tausende von Nutzer:innen des Messenger gewechselt. [Eine Einfürhung in drei Arten von Messengern [EN]](https://thilobuchholz.de/2021/02/3-types-of-messengers-simply-explained/).

Im Februar 2021 start das Jahr von OX. Beim Berlin XMPP Treffen wurde das neue Jahr mit [einem Einführungsvortrag[EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup/4d69d3cc-f28f-465a-baf2-0696024dc9ef) über ["XEP-0373: OpenPGP for XMPP" [EN]](https://xmpp.org/extensions/xep-0373.html) und ["XEP-0374: OpenPGP for Instant Messaging" [EN]](https://xmpp.org/extensions/xep-0374.html) gefeiert. Gesprochen haben eine Gruppe von Expert:innen, namentlich DebXWoody (Umsetzer von OX in Profanity), defanor (Umsetzer von XMPP in rexmpp), Florian (Co-Autor des OX-Standards) und Paul (Umsetzer von OX in Smack). Obwohl die Präsentation nicht aufgenommen wurde, gibt es [Notizen von dem Vortrag [EN]](https://hackmd.io/Ig2azxzUTQuFxN73Ds8Qig) zur öffentlichen Einsicht.

Eine kurze Anleitung zum Einrichten von Siskin-IM in [Englisch](https://xmppeinrichtung.blogspot.com/2021/01/installation-and-configuration-of.html) und [German](https://xmppeinrichtung.blogspot.com/2021/01/installation-and-configuration-of.html).

Eine Anleitung um [einen XMPP-Server mit Prosody aufzusetzen](https://toutetrien.lithio.fr/article/installer-son-serveur-xmpp-avec-prosody) [FR].

[Mellium](https://mellium.im) hat einen [jährlichen Fortschrittsbericht](https://blog.samwhited.com/2021/02/mellium-year-in-review/) für 2020 genauso wie ein [Architekturüberblick](https://blog.samwhited.com/2021/02/mellium-architecture/) veröffentlicht, um neuen Beitragenden zu helfen, sich im Quelltext zurechtzufinden.

Sam Whited hat etwas Dokumentation zum Parsen und Validieren von XMPP-Adressen (JIDs) als [Blogbeitrag [EN]](https://blog.samwhited.com/2021/02/xmpp-addresses/) wiederveröffentlicht.

Gedanken zu [dezentralisierten Forges:Verteilung der Mittel der digitalen Produktion[EN]](https://staticadventures.netlib.re/blog/decentralized-forge/) beinhalten viel zu Protokollen und Technik und XMPP wird erwähnt (OAuth and Salut-a-Toi).


## Software news

### Clients und Anwendungen

[Conversations 2.9.7 wurde veröffentlicht [EN]](https://github.com/iNPUTmice/Conversations/blob/master/CHANGELOG.md), hinzugekommen ist die Möglichkeit den Klingelton für eingehende Anrufe auszuwählen. Außerdem wurde das Erkennen von OpenPGP-Schlüsseln verbessert und die Stabilität von RTP-Sitzungen (Anrufe) verbessert und mehr.

[Gajim 1.3.0 [DE]](https://gajim.org/de/post/2021-02-08-gajim-1.3.0-released/) und [1.3.1 [DE]](https://gajim.org/de/post/2021-03-01-gajim-1.3.1-released/) wurde veröffentlicht. Fünf Monate sind seit der Gajim 1.2.2 vergangen. Viele neue Funktionen wurden in dieser Zeit entwickelt, dazu zählt auch eine komplette Neugestaltung des Einstellungs-Fensters, des Konfigurations-Backends, des Profil-Fensters, neue Unterstützung für Chat-Marker, ein neues Fenster für Audio- und Video-Telefonate und einiges mehr. Wie in den [Entwicklungs-Nachrichten von Februar](https://gajim.org/de/post/2021-02-25-development-news-february/) beschrieben, wurden in diesem Monat hauptsächlich Fehlerbehebungen umgesetzt. Die Fehlerbehandlung des Profil-Fensters wurde verbessert und die Wahl von kleinen Bildern als Avatar sollte nun wie erwartet funktionieren. Während diese Fehler behoben wurde, hat Gajims Team an etwas Großem gearbeitet.

[Kaidan wurde in Version 0.7 freigegeben [EN]](https://www.kaidan.im/2021/02/02/kaidan-0.7.0/): Diese Version ermöglicht es Nutzer:innen Dateien via Drag- and Drop zu senden. Darüberhinaus ist es nun möglich, zu sehen, welchen Client und welches Betriebssystem ein Kontakt verwendet.

[Monal 5 Beta 1 wurde veröffentlicht [EN]](https://monal.im/blog/monal-5-beta-1/), was viele Verbesserungen hinsichtlich OMEMO bringt. Auch wenn es noch in der Beta ist, arbeiten die Monal-Entwickler:innen an der Untersützung von MUC und einer neuen Gestaltung der Dateiübertragung. Monal 5 wird außerdem die [Darstellung von Audio- und Video-Dateien direkt im Chat [EN]](https://monal.im/blog/monal-5-will-support-inline-audio-and-video/) mit sich bringen. Die [Statistiken des Push-Servers vom Januar 2021 [EN]](https://monal.im/blog/monal-stats-1-2021/) zeigen einiges an Aktivität rund um Monal. Die Monal-Entwickler:innen möchten ebenso [das Logo neugestalten [EN]](https://monal.im/blog/redesign-of-the-monal-logo/), also wenn du kreativ bist, fühl' dich frei sie vor dem 31. März auf dich aufmerksam zu machen.

![Vorschau des In-Chat Audio- und Videospielers](/img/blog/xmpp_newsletter_februar_2021/Monal_voicemessage.png "Monal inline A/V")

[Movim 0.19 - Ikeya ist fertig [EN]](https://mov.im/?node/pubsub.movim.eu/Movim/944ac1f8-8034-45ac-976a-cc5fcd35d63e)!. Mit vielen Geschwindigkeitsverbesserungen und vielen neuen Funktionen, wie GIF in Chats, Nachrichten-Antworten, einer Neugestaltung des Community-Browsings und vielem mehr.

Das PeerTube-Plugin Livechat [verwendet Converse.js [EN]](https://github.com/JohnXLivingston/peertube-plugin-livechat).

[SiskinIM 6.3 wurde veröffentlicht [EN]](https://github.com/tigase/siskin-im/releases/tag/6.3) und beinhaltet viele Verbesserungen (klicken auf Links, Abstürze im Zusammenhang mit Markdown-Formatierung, Teilen in öffentlichen Gruppen, Fehler mit der Nachrichten-Korrektur und Verbesserungen beim Layout von Gruppenchats).
 
## Server

[Snikkets Server-Version vom Februar 2021 [EN]](https://snikket.org/blog/feb-2021-server-release/) bringt ein schickes, neues Web-basiertes Admin-Dashboatd, welches dir als Admin alle möglichen Verwaltungs-Aufgaben ermöglicht. Es fügt außerdem ein Portal für nicht-administrative Nutzer:innen hinzu, um deren Konto nach der Anmeldung zu verwalten. Bisher umfasst das ein Profil-Editor und es wird um andere Konto-Einstellungen und Datenimport und -export erweitert werden. Und da ist noch mehr: Raspberry Pi und ARM-Unterstützung sowie die Einführung von Nutzer:innengruppen, genannt "Circles".

![Snikket Web-Admin Ansicht](/img/blog/xmpp_newsletter_februar_2021/snikket-web-admin-multilingual.png "Snikket web admin view")

[Prosody 0.11.8 wurde freigegeben [EN]](https://blog.prosody.im/prosody-0.11.8-released/). Diese Version bringt Fehlerbehebungen, Verbesserungen bei der WebSocket-Geschwindigkeit (dank der Leute von Jitsi) und einen Sicherheits-Fix.

[ejabberd 21.01 [EN]](https://www.process-one.net/blog/ejabberd-21-01/) wurde veröffentlicht und beinhaltet viele Verbesserungen und Fehlerbehebungen (zum Beispiel STUN/TURN Standard-Blocklisten, bessere systemd-Integration). Server-Admins können die neue Version wie bisher von der Entwickler:innen-Website beziehen und für Debian-Nutzer:innen steht das Paket im `Buster-Backports` Repositorium bereit.

Die Ignite Realtime Gemeinschaft freut sich die [Freigabe von Version 0.8.0 des Push-Benachrichtigungs Plugins für Openifre [EN]](https://discourse.igniterealtime.org/t/push-notification-openfire-plugin-0-8-0-released/89657)! anzukündigen. Diese Aktualisierung löst ein Problem, das bestimmte Clients (wie Siskin) daran gehindert hat, sich erfolgreich bei einem Push-Benachrichtigungs-Dienst zu registrieren.

## Bibliotheken

[Mellium v0.18.0 wurde veröffentlicht [EN]](https://pkg.go.dev/mellium.im/xmpp@v0.18.0), mit überarbeiteten Verbindungs-APIs, besserer Server-Unterstützung, Websockets und der normalen Anzahl an Fehlerbehebungen und Nutzungsverbesserungen. Um die volle Liste einzusehen, ließ das [Änderungsprotokoll [EN]](https://github.com/mellium/xmpp/releases/tag/v0.18.0).

[python-nbxmpp 2.0.1 und 2.0.2 wurden freigegeben [DE]](https://gajim.org/de/post/2021-02-25-development-news-february/), beide beheben einige Fehler.

## Erweiterungen und Spezifikationen

Entwickler:innen und andere Standard-Expert:innen von der ganzen Welt arbeiten an diesen Erweiterungen zusammen, entwicklen neue Spezifikationen um Methoden zu verbreiten und die bisher etablierten Wege, Dinge zu tun, weiterzubringen. Von jeder und jedem vorgeschlagen, erhalten die besonders erfolgreichen den Status final oder aktiv - je nach Typ - zu werden, während andere zurückgestellt werden. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und vorschriftsmäßigen Definitionen für Typen, Beschaffenheiten und Prozessen beinhalten. [Erfahre mehr über den Standards-Prozess [EN]](https://xmpp.org/about/standards-process.html). Kommunikation rund um Standards und Erweiterungen findet in der [Standards Mailingsliste [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt ([Online-Archiv [EN]](https://mail.jabber.org/pipermail/standards/)).

### Vorgeschlagen

Der XEP Entwicklungs-Prozess startet damit, eine Idee aufzuschreiben und sie dem XMPP-Editor zu übermitteln. Innerhalb von zwei Wochen entscheidet das Führungsgremium, ob der Vorschlag als experimentelles XEP aufgenommen wird.

- [Implicit XMPP WebSockets Endpoints [EN]](https://xmpp.org/extensions/inbox/xep-iwe.html)
    - Dieses Dokument beschreibt implizite Verbindungspunkte von XMPP over WebSockets (RFC 7395).

### Neu

-   Version 0.2.0 von [XEP-0455 [EN]](https://xmpp.org/extensions/xep-0455.html) (Service Outage Status)
    -   Dieses Dokument definiert eine XMPP-Protokollerweiterung, die Server-Admins ermöglicht, Fehler mit dem Server in einem semantischen Weg an alle Nutzer:innen zu kommunizieren

### Zurückgestellt

Wenn ein experimentelles XEP für mehr als sechs Monate nicht aktualisiert wurde, wird es von experimentell zu zurückgestellt verschoben. Gibt es eine weitere Aktualisierung, wird das XEP wieder zu experimentell verschoben.

-  Diesen Monat wurde keine XEPs zurückgestellt

### Aktualisiert

-   Version 0.2.0 von [XEP-0452 [EN]](https://xmpp.org/extensions/xep-0452.html) (MUC Mention Notifications)
    -   Nickname erfordern, damit Benachrichtigungen funktionieren (jcb)

### Letzter Aufruf

Letzte Aufrufe werden ausgegeben, sobald jede:r mit dem aktuellen Status eines XEP zufrieden zu sein scheint. Nachdem der Führungsrat entscheidet, ob das XEP fertig erscheint, ruft der XMPP-Editor zu einem letzten Aufruf für Kommentare auf. Das Feedback, das während des letzten Aufrufes gesammelt wird, kann verwendet werden, um das XEP zu verbessern, bevor es zum Führungsrat zurückübermittelt wird, um es zum Entwurf aufzuwerten.

-   Keine neuen letzten Aufrufe diesen Monat.

### Draft

-   Keine Entwürfe diesen Monat.

### Aufruf für Erfahrungen

Ein Aufruf für Erfahrungen - wie ein letzter Aufruf - ist ein explizieter Aufruf für Kommentare, aber in diesem Fall meistens direkt an die Menschen, die die Spezifikation implementiert und idealerweise ausgerollt haben. Das Führungsgremium stimmt dann über die Verschiebung zu final ab.

- Kein Aufruf für Erfahrungen diesen Monat.

## Danke an alle!

Dieser XMPP-Newsletter ist kollaborativ bei der Gemeinschaft erstellt.

Danke an wurstsalat3000, Sam Whited, pitchum, Licaon_Kter, jeybe und emus für deren Hilfe bei der Erstellung!

## Teile die Neuigkeiten!

Bitte teile die Neuigkeiten auf "sozialen Netzwerken":

* [Twitter](https://twitter.com/xmpp)
* [Mastodon](https://fosstodon.org/@xmpp/)
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook](https://www.facebook.com/jabber/)
* [Reddit](https://www.reddit.com/r/xmpp/)

Finde und stelle Jobs in das [XMPP job board](https://xmpp.work/).

[Abonniere des englischen XMPP-Newsletter](https://tinyletter.com/xmpp) und erhalte die nächste Ausgabe direkt in dein Postfach, sobald sie verfügbar ist!

Schau dir auch unseren [RSS Feed [EN]](https://xmpp.org/feeds/all.atom.xml) an!

## Sei Behilflich bei der Erstellung des Newsletters

Wir freuen uns immer über Beitragende. Zögere nicht, der Diskussion in unserem [Comm-Team Gruppenchat (MUC) [EN]](xmpp:commteam@muc.xmpp.org?join) beizutreten und uns damit zu helfen, dies als Gemeinschafts-Projekt aufrechtzuerhalten.

Du hast ein Projekt und schreibst darüber? Bitte denk' doch darüber nach, die Neuigkeiten oder Veranstaltungen hier zu teilen und es einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neugikeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

