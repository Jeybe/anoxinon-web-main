+++
date = "2018-07-24T00:19:00+01:00"
title = "Aufschaltung Halcyon"
tags = ["mastodon","rss","anoxinon","fediverse","halcyon"]
categories = ["dienste"]
author = "Anoxinon"
draft = false
description = "UI für Mastodon und Pleroma"
+++
Sicherlich gibt es einige unter Euch, die zu Mastodon gewechselt sind aber das Twitter-Feeling vermissen. Hierfür haben wir eine Lösung gefunden und möchten diese gerne mit Euch teilen: <b>Halcyon</b>.

Halcyon bietet Mastodon- wie Pleroma-Nutzern eine Oberfläche im gewohnten Twitter Design.
<br><br>
<b>Hierzu ein Hinweis:</b>
Bei Nutzung von Halcyon werden keine Daten auf unseren Servern gespeichert. Halcyon dient ausschliesslich als Transmitter zwischen dem Browser und der Seite Eures Mastodon-Instanz-Betreibers. Da sich Halcyon noch in der Entwicklung befindet, können sich in Zukunft noch Änderungen ergeben.<br><br>

<div id="action-buttons">
  <a class="button primary big" href="https://halcyon.anoxinon.de">Zu Halcyon</a>
    <p><a href="https://notabug.org/halcyon-suite/halcyon/">Source Code</a></p>
    </div>
Wenn Ihr generell Fragen zu Mastodon und dem Fediverse habt,<br> findet Ihr hier weitere Antworten:
<a href="https://anoxinon.de/dienste/anoxinonsocial/"> Klickt hier für mehr Infos!</a>

Aber auch wir stehen Euch gerne Rede und Antwort.

Viele Grüße,
<br>
euer Anoxinon Team
