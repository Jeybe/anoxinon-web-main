+++
date = "2020-08-02T19:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #07"
description = "Aktuelles"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten", "Website", "Verein"]

+++

Hallo zusammen!

Heute gibt es nach einer längeren Durststrecke mal wieder Infos zum Verein.

---

###### Neuigkeiten aus dem Vereinsleben

Zuerst die schlechte Nachricht; leider hat Cedric B. den Verein am 01.08.2020 verlassen. Er war Mitbegründer, Beirat, Verwaltung und Ansprechpartner für sehr viele Aufgabengebiete und Themen. Wir können an dieser Stelle gar nicht alles aufzählen was er geleistet hat. Eines ist sicher, ohne ihn wäre der Verein nicht möglich gewesen und sein Ausscheiden schmerzt uns sehr. Natürlich hoffen wir, dass er eines schönen Tages wieder den Weg zu Anoxinon finden wird. Bis dahin wünschen wir ihm alles erdenklich Gute, viel Freude und Erfolg bei allem was noch kommen mag. Vielen Dank für einfach alles Cedric!

---

###### Aktuelles Jahr

Umstrukturierung innerhalb des Vorstandes, private Angelegenheiten, die viel Zeit kosteten und nicht zuletzt COVID-19, hatten leider wieder ihren Tribut gefordert. 

Offline Aktivitäten gab und gibt es dieses Jahr dank COVID-19 leider nicht. Aktuell können wir so nicht wirklich planen. Aber wir möchten natürlich weiter wachsen und für die Zukunft auch mehr Dienste bereit stellen, ob digital oder real. Wenn ihr Teil des Teams werden wollt, hier gehts lang: "[Mitmachen](https://anoxinon.de/mitmachen/)" 

Nun zu den positiven Nachrichten, das Wichtigste zuerst: unsere Homepage im neuen Design ist fertig und online. Schaut mal rein und gebt uns Feedback; vielen Dank an unsere fleißigen Coder und Texter, ohne euch wäre das nicht möglich gewesen!

Des Weiteren hat der Verein 4 neue Mitglieder hinzugewonnen. Herzlich willkommen und danke für eure Unterstützung!


---

###### Personelles/Finanzen

Für den Verein sind derzeit 14 ehrenamtliche Mitarbeiter in diversen Bereichen tätig, exklusive des Vorstandes. Die Mitgliederzahl beläuft sich auf 20.


---

###### Sonstiges

Leider musste unser diesjähriges RL-Vereinstreffen aufgrund der COVID-19 Beschränkungen ausfallen..

Da hätten wir doch fast vergessen, den DebXWoody und seine neuesten Coding Projekte, zu erwähnen :(.
Für alle Liebhaber von XMPP im Terminal gibt es jetzt "[xmppc](https://codeberg.org/Anoxinon_e.V./xmppc)" sowie "[eagle](https://codeberg.org/DebXWoody/eagle)". Ein "[Handbuch für XMPP](https://codeberg.org/Anoxinon_e.V./anoxinon-xmpp-handbuch)" ist ebenfalls in Planung. Unter "[Codeberg - DebXWoody](https://codeberg.org/DebXWoody)" könnt ihr noch weitere, interessante Projekte von ihm entdecken.

---

Soweit soll es das erst mal gewesen sein.  
Bei Fragen dürft Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße

das Anoxinon Team
 
