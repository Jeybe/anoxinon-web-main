--- 
date: "2021-11-10T23:59:59+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter Oktober 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Oktober 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---


*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/11/the-xmpp-newsletter-october-2021/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat Oktober 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:

- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, das Newsletter Team zu unterstützen? Ließ mehr am Ende des Beitrags!

Abseits davon - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news.jabberfr.org/category/newsletter/)
- Die italienische Version des letzten Monats auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)

Vielen Dank an die Übersetzer:innen und deren Arbeit! Das ist eine große Hilfe dabei, unsere Neuigkeiten zu verbreiten! Mach doch bitte auch mit oder beginne mit einer weiteren Sprache!

## Ankündigungen

- Die XSF-Mitglieder müssen bald das kommenden XSF-Board und den -Council wählen. Wenn du daran interessiert bist, für das XSF-Board (geschäftlich) oder den XSF-Council (technisch) für die Amtszeit 2021-2022 zu kandidieren, [erstell' bitte bis zum 7. November 2021 eine entsprechende Wiki-Seite und verlinke sie von hier [EN]](https://wiki.xmpp.org/web/Board_and_Council_Elections_2021)
- Die XSF bietet jetzt [steuerliches (fiscal) Hosting [EN]](https://xmpp.org/community/fiscalhost/) für XMPP-Projekte an! Bitte bewirb dich über [Open Collective [DE]](https://opencollective.com/xmpp). Weitere Informationen im [Blogbeitrag zur Ankündigung [EN]](https://xmpp.org/2021/09/the-xsf-as-a-fiscal-host/).
	- Außerdem hat die XSF ihr erstes Projekt in das Fiscal-Hosting-Programm aufgenommen! Ein herzliches Willkommen an das [MAM-Plugin für XMPP.js [EN]](https://gitlab.com/deblounge/xmppjs-mam-plugin) (bitte beachte, dass es nicht mit dem Upstream-Projekt [xmpp.js [EN]](https://github.com/xmppjs/xmpp.js) zu tun hat)! Sie werden daran arbeiten, xmpp.js mit MAM-Unterstützung auszustatten, mit dem eventuellen Ziel, es der Matrix Bifrost Bridge hinzuzufügen, damit XMPP-Benutzer:innen den Verlauf ihrer bevorzugten Matrix-Kanäle abrufen können. Du kannst [hier für diese Bemühungen spenden [DE]](https://opencollective.com/bifrost-mam).
- Die XSF plant die Teilnahme am Google Summer of Code 2022 (GSoC). Wenn du daran interessiert bist, als Student:in, Mentor:in oder als Projekt im Allgemeinen teilzunehmen, [füge bitte deine Ideen hinzu und melde dich bei uns [EN]](https://wiki.xmpp.org/web/Google_Summer_of_Code_2022)!
- Die Blog- und Newsletter-Seiten auf [xmpp.org/blog [EN]](https://xmpp.org/blog) unterstützen jetzt mehrere Sprachen. Wir freuen uns über Freiwillige, die uns beim Übersetzen unterstützen!

## Veranstaltungen

[XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Schau auch auf unserem neuen  [YouTube-Kanal [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbei!

[Berlin XMPP Meetup (remote) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Monatliches Treffen von XMPP Enthusiast:innen in Berlin - jeden zweiten Mittwoch des Monats.

## Videos

Sam Whited hielt bei den [XMPP Office Hours einen Vortrag [EN]](https://www.youtube.com/watch?v=lprIwxyPY2E) über den neuen Fiskal-Hosting-Service der XSF!

Gastvortrag beim Berliner XMPP Meetup: ["Diving deep into Briar at the XMPP Meetup Berlin" [EN]](Diving deep into Briar at the XMPP Meetup Berlin)

## Artikel

Nachdem wir im Newsletter vom [September 2021 [DE]](/blog/xmpp_newsletter_september_2021/) einen Vergleich der Webclients von Movim und Element vorgestellt haben, ist [Ade Malsasa Akbar [EN]](https://floss.social/@ademalsasa) nun mit [einem kurzen Tutorial [EN]](https://www.ubuntubuzz.com/2021/10/how-to-voice-video-calls-on-xmpp-and-matrix-made-simple.html) über die Verwendung von Audio- und Videoanrufen in diesen Clients zurück.

Hast du schon von [Bad XMPP [EN]](https://badxmpp.eu/) gehört? Nun, es ist in Wirklichkeit gut - du kannst deine Clients und Server gegen eine Reihe von absichtlich schlecht konfigurierten XMPP-Diensten testen. Viel Spaß beim Debuggen!

![Screenshot der Bad XMPP Webseite. Man sieht beschrieben, dass man auf schlecht konfigurierte XMPP Services testen kann.](/img/blog/xmpp_newsletter_oktober_2021/bad_xmpp.png)

Der jmp.chat Service Blog [kündigt einige Änderungen an der SIP-Funktion für eingehende Anrufe an [EN]](https://blog.jmp.chat/b/october-newsletter-2021), so dass es den Benutzer:innen empfohlen wird, diese über den neuen interaktiven Bot zu konfigurieren. Auf der iOS-Seite ist Snikket nun die empfohlene App, da einige hilfreiche Funktionen (wie Ruftöne) gesponsert wurden.

Aria Network schrieb einen weiteren Blogbeitrag über [UX-Verbesserungen wie die Avatare der Bifrost-Brücke zu Matrix [EN]](https://aria-net.org/SitePages/Bifrost-Improvements.aspx). Der vorherige Artikel lautete  [“Conversion of communities to spaces, new Bifrost features” [EN]](https://aria-net.org/SitePages/Regarding-spaces-and-Bifrost.aspx).

## Software News

### Clients und Apps

Nach den Conversations- und Quicksy-Veröffentlichungen von 2.10.0 im letzten Monat enthält die Version [2.10.1 [EN]](https://github.com/iNPUTmice/Conversations/releases/tag/2.10.1) eine neue interne Bibliothek für die Videokompression, die es mehr Geräten ermöglicht, kleinere Dateien zu senden. Konversationen werden nun in Android-Backups (sei es von Seedvault oder Google) enthalten sein, allerdings nur, wenn sie verschlüsselt sind. Darüber hinaus wurden Korrekturen am Adressbuch vorgenommen, so dass Kontaktadressen nun leichter aufgenommen werden können. Schließlich gibt es Korrekturen für Benachrichtigungen, die es ermöglichen, Downloads richtig zu starten und MUC PMs an den richtigen Kontakt zu senden.

[Gajim 1.3.3 [EN]](https://gajim.org/de/post/2021-10-10-gajim-1.3.3-released/) wurde veröffentlicht. Diese Version bietet verbesserte Ad-Hoc-Befehle und bringt die Rechtschreibprüfung zurück. Gajim 1.3.3 enthält viele Fehlerkorrekturen und Verbesserungen. Außerdem in diesem Monat: zwei Jahre Gajim-Entwicklungsnachrichten! Im Oktober wurde das Ereignissystem von Gajim erheblich verändert. Diese Änderungen führten vor allem zu Verbesserungen bei den Benachrichtigungen, ermöglichten es aber auch, Jingle File Transfers direkt in der Chat-Ansicht anzuzeigen. Eine weitere nette Ergänzung: Suchfilter, die helfen, die Suche einzugrenzen.

[Version 2.10.2 der Snikket Android App wurde veröffentlicht [EN]](https://fosstodon.org/@snikket_im/107161786225773950)! Das Highlight dieser Version ist das neue Dialpad, mit dem man durch automatische Anrufsysteme navigieren kann, wenn man eine Jingle to PSTN Bridge benutzt.

Neue Beta-Versionen von [Beagle IM und Siskin IM wurden für MacOS und iOS mit Unterstützung für Location Sharing veröffentlicht [EN]](https://mastodon.technology/@tigase/107161741669699519).

![Screenshot vom Beagle-IM, das eine Karte zeigt. Eine Stecknadel zeigt den Standort eines Kontakts.](/img/blog/xmpp_newsletter_oktober_2021/beagle-location.jpg)

### Server

Das Openfire Plugin für Push Notification hat einen kleinen Bugfix erhalten, so dass Serveradministrator:innen empfohlen wird, auf [die neue Version 0.9.0 [EN]](https://discourse.igniterealtime.org/t/push-notification-openfire-plugin-0-9-0-released/90934) zu aktualisieren.


### Libraries (Bibliotheken)

Die Entwicklung von [mellium.im/xmpp [EN]](https://mellium.im/xmpp) (eine XMPP-Bibliothek in Go) verlief diesen Monat langsamer als sonst. Wir haben uns hauptsächlich darauf konzentriert, die grundlegende Unterstützung für pubsub, PEP und PEP Native Bookmarks zu implementieren, und die Grundlagen sind bereits in den Main Branch eingeflossen. Weitere Informationen findest du im [letzten Dev Communiqué [EN]](https://opencollective.com/mellium/updates/dev-communique-for-october-2021).


## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwickeln neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

* Diesen Monat werden keine XEP vorgeschlagen.

### New (Neu)

* Diesen Monat gibt es keine neuen XEPs.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

* Diesen Monat wurden keine XEPs zurückgestellt.

### Updated (Aktualisiert)

- Version 0.8.0 von [XEP-0392 [EN]](https://xmpp.org/extensions/xep-0392.html) (Consistent Color Generation)
    - Algorithmen zur Korrektur von Farbsehschwächen entfernen und durch eine bessere Empfehlung ersetzen. (jsc)
- Version 0.5 von [XEP-0355 [EN]](https://xmpp.org/extensions/xep-0355.html) (Namespace Delegation)
    - Delegation der verbleibenden Discovery Infos
    - Delegation von Bare JID Disco Items
    - Sicherheitsbetrachtung über Disco-Anfragen
    - Namespace Bump
    - Tippfehler (jp)
- Version 0.8.0 von [XEP-0313 [EN]](https://xmpp.org/extensions/xep-0313.html) (Message Archive Management)
    - Aktualisierung der Gruppenchat-Nachrichten-in-Nutzer-Archiv Hinweise, Einführung von Feldern und Disco-Features, um das Verhalten in zukünftigen Implementierungen explizit zu machen, im Lichte des Last Call Feedbacks. (ks)
- Version 0.4.0 von [XEP-0450 [EN]](https://xmpp.org/extensions/xep-0450.html) (Automatic Trustmanagement (ATM)) 
    - Aktualisierung auf [XEP-0434 [EN]](https://xmpp.org/extensions/xep-0434.html) Version 0.6.0 und [XEP-0384 [EN]](https://xmpp.org/extensions/xep-0384.html) Version 0.8.0
    - Verwendung von Base64-kodierten Schlüsselbezeichnern in Beispielen
    - Aktualisierung des TM-Namensraums auf urn:xmpp:tm:1
    - Aktualisierung des OMEMO-Namensraums auf urn:xmpp:omemo:2 (melvo)
- Version 0.6.0 von [XEP-0434 [EN]](https://xmpp.org/extensions/xep-0434.html) (Trust Messages (TM))
    - Spezifizierung der Kodierung der Schlüsselbezeichner, Verbesserung des Glossars und Aktualisierung auf [XEP-0384 [EN]](https://xmpp.org/extensions/xep-0384.html) Version 0.8.0
    - Spezifizierung der Verwendung von Base64-Kodierung für Schlüsselbezeichner in Vertrauensnachrichten
    - Spezifizierung der Verwendung der Base16-Kodierung für Schlüsselbezeichner in Vertrauensnachrichten-URIs
    - Verwendung von Base64-kodierten Schlüsselbezeichnern in Beispielen
    - Hinzufügen von "Hash-Wert" als Beispiel für Schlüsselbezeichner
    - Aktualisierung des OMEMO-Namensraums auf urn:xmpp:omemo:2
    - Aktualisierung des Namensraums auf urn:xmpp:tm:1 (melvo)
- Version 0.5.0 von [XEP-0401 [EN]](https://xmpp.org/extensions/xep-0401.html) (Erzeugung von Ad-hoc-Kontoeinladungen)
    - Faktor aus [XEP-0445 [EN]](https://xmpp.org/extensions/xep-0445.html) (gl)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

- [XEP-0379 [EN]](https://xmpp.org/extensions/xep-0379.html) Pre-Authenticated Roster Subscription (Vorauthentifiziertes Roster-Abonnement)
- [XEP-0401 [EN]](https://xmpp.org/extensions/xep-0401.html) Ad-hoc Account Invitation Generation (Erzeugung von Ad-hoc-Kontoeinladungen)
- [XEP-0445 [EN]](https://xmpp.org/extensions/xep-0445.html) Pre-Authenticated In-Band Registration (Vorauthentifizierte In-Band-Registrierung)

### Stable (Stabil)

Info: Die XSF hat beschlossen, "Draft" in "Stable" umzubenennen. [Ließ hier mehr darüber [EN]](https://github.com/xsf/xeps/pull/1100).

- Version 1.0.0 of [XEP-0280[EN]](https://xmpp.org/extensions/xep-0280.html) (Message Carbons)
    - Gemäß Ratsbeschluss vom 29.09.2021 zu "Stable" machen. Unglaublich. (jsc (XEP Editor))

### Deprecated (Veraltet)

- Version 1.1.0 von [XEP-0411 [EN]](https://xmpp.org/extensions/xep-0411.html) (Konvertierung von Lesezeichen)
    - Veraltet durch Abstimmung des Rates am 06.10.2020 (XEP Editor (jsc))

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

* Kein Call for Experience in diesem Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an Adrien Bourmault (neox), Anoxinon e.V., Benoît Sibaud, emus, Julien Jorge, Licaon_Kter, MattJ, mdosch, Nicola Fabiano, seveso, Sam Whited, SouL, wojtek, wurstsalat3000, Ysabeau ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook [EN]](https://www.facebook.com/jabber/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

