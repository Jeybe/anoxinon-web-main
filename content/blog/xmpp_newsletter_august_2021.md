--- 
date: "2021-10-10T19:30:59+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter August 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den August 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/09/the-xmpp-newsletter-august-2021/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat August 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:

- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, das Newsletter-Team zu unterstützen? Ließ mehr weiter unten!

Abseits davon - viel Spaß beim Lesen?

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [jabberfr.org](https://news.jabberfr.org/category/newsletter/) und [linuxfr.org](https://linuxfr.org/tags/xmpp/public)
- Die italienische Version des letzten Monats auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)

## Veranstaltungen

[XMPP Office Stunden jede Woche](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Schau auch auf unserem [YouTube-Kanal](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbei!

[Berlin XMPP Meetup (remote)](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Monatliches Meeting von XMPP-Enthusiast:innen in Berlin - immer am zweiten Mittwoch eines jeden Monats.

## Artikel

Was ist das Projekt XPORTA? Wie im [Newsletter vom April '21](/blog/xmpp_newsletter_april_2021) angekündigt, sponsert der Data Portability and Services Incubator bei NGI das XMPP Account Portability Projekt namens XPORTA. Diesen Monat gibt es ein Interview mit Matthew Wild über die Entstehung dieses Projekts.

Der "mache deine eigene Telefonkonferenz basierend auf XMPP"-Dienst [jmp.chat [EN]](https://jmp.chat/) hat einen neuen Blog, mit einem Twist, jetzt basierend auf Libervia, also basierend auf XMPP, mit all den netten Blog-Funktionen, die du möchtest (wie RSS) und sogar Abonnements über XMPP (mit kompatiblen Clients wie Movim oder Libervia). [Der Beitrag, der den neuen Blog ankündigt [EN]](https://blog.jmp.chat/b/august-newsletter-2021), behandelt auch den neuen Registrierungsablauf und das Abrechnungssystem. Aber der vorige Beitrag ist das eigentliche Juwel, genannt [Adventures in WebRTC: Making Phone Calls from XMPP [EN]](https://blog.jmp.chat/b/adventures-in-webrtc-making-phone-calls-from-xmpp-znLOT5). Er beschreibt die Reise durch WebRTC-Debugging, mehrere Clients, NAT, ICE und die Überwachung durch Wireshark. Hol dir ein heißes oder kaltes Getränk zu dieser etwa 70 Minuten langen Lektüre.

Im [letzten Newsletter](/blog/xmpp_newsletter_juli_2021) haben wir erwähnt, dass Debian Linux 11 bald mit aktualisierter XMPP-Software auf den Markt kommen wird, da dies in der Zwischenzeit geschehen ist, aktualisieren Server-Administratoren bereits oder richten sogar neue Deployments ein. So wie Nelson aus Luxemburg, der einen Blogbeitrag über die [Einrichtung eines Servers mit ejabberd auf Debian 11 Bullseye [EN]](https://www.aroundtheglobe.biz/posts/20210819-your_own_xmpp_server_with_ejabberd_on_Debian_11_Bullseye.html) veröffentlicht hat.

Während die Snikket-iOS-Client-App gerade veröffentlicht wurde (mehr dazu weiter unten), geht die Entwicklung hinter den Kulissen weiter. In seinem letzten Blogbeitrag kündigt Matthew Wild an, dass [die Expert:innen von Simply Secure ein Usability-Audit der aktuellen App durchführen werden [EN]](https://snikket.org/blog/simply-secure-collaboration/), sowie Usability-Tests dank der Finanzierung durch das OTF Usability Lab. Die Analyse wird dazu beitragen, die Benutzerfreundlichkeit der iOS-App und von Snikket als Ganzes zu verbessern.

In der letzten Ausgabe vermisst, haben die Leute von [cometchat [EN]](https://www.cometchat.com/) in ihrem Blog [Everything About XMPP - Extensible Messaging & Presence Protocol [EN]](https://www.cometchat.com/blog/xmpp-extensible-messaging-presence-protocol) einen Überblick über die Geschichte von XMPP, die Arbeitsarchitektur, die Stanzas und die Funktionen im Allgemeinen gegeben. Wenn du einen schnellen technischen Überblick möchtest (oder einen brauchen, um anderen zu zeigen, worum es bei XMPP geht), kann diese ~15 Minuten lange Lektüre einen auf den neuesten Stand bringen.

"Spaces" sind die neue XMPP-Grenze, die es zu erforschen gilt, und in den unten stehenden Gajim-Client-Nachrichten erhälst du einen Eindruck davon, aber die Arbeit ist ziemlich aufwendig und wird von vielen Beteiligten fortgesetzt. Der Entwickler von [Renga [EN]](https://github.com/HaikuArchives/Renga) (ein XMPP-Client für Haiku), [pulkomandy [EN]](https://pulkomandy.tk/_/_.users/_pulkomandy), hat in einem Blog [einige zufällige Gedanken über XMPP-Räume [EN]](https://pulkomandy.tk/_/_Development/_Some%20random%20thoughts%20about%20XMPP%20spaces) geäußert, in denen er über Anwendungsfälle (Familie, Unternehmen, Gemeinschaften) und Benutzerschnittstellen nachdenkt.

Gibt es türkischsprachige Leser des Newsletters? Wir haben noch keine Übersetzung, aber [Ged [EN]](https://fosstodon.org/@Ged) hat gerade einen ausführlichen Blogbeitrag über XMPP mit dem Titel [Hangi "Chat" Programı [TR]](https://www.yunbere.kalfazade.com/post/hangi-chat-programi?) veröffentlicht. In etwa 40 Minuten führt er den Leser durch die Geschichte des Protokolls, berichtet über Anwendungen, Server, Vergleiche mit beliebten Anwendungen und Datenschutz.

Der [Newsletter vom März `21](/blog/xmpp_newsletter_märz_2021) brachte die Nachricht, dass JSXC (der Javascript XMPP Client) eine Finanzierung für die Arbeit an Gruppen-Chat-Anrufen erhalten hat. In diesem Monat [berichten sie [EN]](https://www.jsxc.org/blog/2021/08/31/A-group-call-proposal.html) über die geleistete Arbeit und erläutern die aktuellen Fortschritte, die sogar getestet werden können.

Und schließlich: Wie funktioniert FaceTime? [Interessanterweise verwenden sie denselben Port (5223) wie XMPP... [EN]](https://matduggan.com/how-does-facetime-work/)

## Software News

### Clients und Apps

[Gajim 1.4 Vorschau: Workspaces](https://gajim.org/de/post/2021-08-27-workspaces/). Das Gajim-Team hat in den letzten Monaten hart an der Vorbereitung der nächsten Version 1.4 gearbeitet. Die kommende Version bringt eine umfassende Neugestaltung der Benutzeroberfläche mit sich. In diesem Beitrag wird erklärt, wie die neue Oberfläche funktioniert und was vor der Veröffentlichung noch entschieden oder implementiert werden muss.

![Gajim Workspaces (Vorschau)](/img/blog/xmpp_newsletter_august_2021/gajim-workspaces-preview.png)

[Der Libervia-Fortschrittsbericht 2021-W31 [EN]](https://www.goffi.org/b/libervia-progress-note-2021-w31-Ua14) enthält Informationen über die Docker-Integration, das Übersetzungsportal und die erste 0.8.0-Beta. Sie enthält auch viele Details über die Arbeit am ActivityPub Gateway Projekt (Grant wurde im [April '21 Newsletter angekündigt](/blog/xmpp_newsletter_april_2021)) mit SQL, DBus, PubSub und mit neuen und aktualisierten XEPs.

Communiqué ist ein neuer XMPP-Client des [Mellium Co-op Teams [EN]](https://opencollective.com/mellium). Er wurde diesen Monat [angekündigt [EN]](https://opencollective.com/mellium/updates/introducing-communique) und bei den XMPP Office Hours vorgestellt (leider hat die Aufzeichnung nicht geklappt). Der Quellcode kann [im Repository [EN]](https://github.com/mellium/xmpp/) gefunden werden.

![communiqué](/img/blog/xmpp_newsletter_august_2021/communique.png)

Monal 5.0.1 ist jetzt sowohl für [iOS als auch für macOS [EN]](https://monal.im/blog/monal-5-0-1-synchronized-builds-and-bugfixes/) verfügbar und bringt vor allem Korrekturen und mehr Feinschliff gegenüber dem vorherigen Major Release.

Das JSXC Openfire Plugin ist in [Version 4.3.1-1 [EN]](https://discourse.igniterealtime.org/t/jsxc-openfire-plugin-4-3-1-1-released/90583) erschienen, die hauptsächlich Fehlerbehebungen und Verbesserungen des JSXC-Projekts enthält.

Nach vielen Monaten des Wartens ist [die Snikket iOS App nun öffentlich zugänglich [EN]](https://snikket.org/blog/snikket-ios-public-release/). Snikket-Server-Administratoren können die App zu den Einladungsseiten hinzufügen, damit Apple-Nutzer sie leicht finden können. Wenn Sie Snikket nicht nutzen, können Sie die App trotzdem verwenden (Sie können die Anmeldeinformationen direkt verwenden), aber lesen Sie den Blog-Beitrag, um zu erfahren, was Sie zu Ihrer Prosody-Instanz hinzufügen müssen (Einladungsmodule) oder welche Einschränkungen Sie bei der Verwendung einer anderen Server-Software erfahren könnten.

![Snikket auf iOS](/img/blog/xmpp_newsletter_august_2021/snikket_on_ios.png)


### Server

Prosody 0.11.10 wurde mit einem Fix für CVE-2021-37601 und einigen kleineren Änderungen [veröffentlicht [EN]](https://blog.prosody.im/prosody-0.11.10-released/). Die Prosody-Entwickler empfehlen Server-Administratoren ein Upgrade, um das Problem der Offenlegung von Remote-Informationen zu beheben.

### Libraries (Bibliotheken)

Das Mellium Dev Communiqué für August enthält Aktualisierungen der Mellium XMPP-Bibliothek sowie des neuen Instant-Messaging-Clients Communiqué. Die größten Updates in diesem Monat sind MAM und die Unterstützung von Ad-hoc-Befehlen! [Mehr dazu kannst du hier lesen [EN]](https://opencollective.com/mellium/updates/dev-communique-for-august-2021).

## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

- Keine vorgeschlagenen XEPs diesen Monat

### New (Neu)

- Version 0.1.0 von [XEP-0460 [EN]](https://xmpp.org/extensions/xep-0460.html) (Pubsub Caching Hints)
    - Akzeptiert durch die Wahl des Councils am 2021-07-21 (XEP Editor (jsc))

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

- Keine zurückgestellten XEPs diesen Monat

### Updated (Aktualisiert)

- Version 1.21.0 von [XEP-0060 [EN]](https://xmpp.org/extensions/xep-0060.html) (Publish-Subscribe)
    - Änderungen von Version 1.15.5 rückgängig gemacht, welche "meta-data" zu "metadata" im Übertragungsprotokoll geändert haben. Das war eine ungewollte abwärtsinkompatible Änderung, welche nun zurückgenommen wurde. (pep)
- Version 0.3.0 von [XEP-0214 [EN]](https://xmpp.org/extensions/xep-0214.html) (File Repository and Sharing)
    - Änderungen von Version 0.2.1 rückgängig gemacht, welche "meta-data" zu "metadata" im Übertragungsprotokoll geändert haben. Das war eine ungewollte abwärtsinkompatible Änderung, welche nun zurückgenommen wurde. (rm)
- Version 0.3.0 von [XEP-0248 [EN]](https://xmpp.org/extensions/xep-0248.html) (PubSub Collection Nodes)
    - Änderungen von Version 0.2.1 rückgängig gemacht, welche "meta-data" zu "metadata" im Übertragungsprotokoll geändert haben. Das war eine ungewollte abwärtsinkompatible Änderung, welche nun zurückgenommen wurde. (rm)
- Version 0.2.0 von [XEP-0238 [EN]](https://xmpp.org/extensions/xep-0283.html) (Moved)
    - Den Ablauf mit einem fokussierteren Ansatz neugeschrieben (mw)
- Version 1.1.0 von [XEP-0429 [EN]](https://xmpp.org/extensions/xep-0429.html) (Special Interest Group End to End Encryption)
    - Diskussionsort nach der Erstellung vom Infrastructure Team hinzufügen (mw)
- Version 1.24.0 von [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) (XMPP Extension Protocols)
    - "Draft" zu "Stable" ändern (ssw)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

- Kein Last Call diesen Monat

### Stable (bisher bekannt als Draft)

- Kein Entwurf diesen Monat

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

- Kein Call for Experience diesen Monat

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an  Adrien Bourmault, Benoît Sibaud, DebXwoody, COM8, emus, Jeybe, jk-forensics, mattJ, neox, Licaon_Kter, pmaziere, raspbeguy, wurstsalat3000, Ysabeau für ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook [EN]](https://www.facebook.com/jabber/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

