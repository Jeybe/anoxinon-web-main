--- 
date: "2022-02-10T19:30:00+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter Dezember 2021 und Januar 2022" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Dezember 2021 und den Januar 2022" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2022/02/the-xmpp-newsletter-december-2021-january-2022/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Dezember 2021 und den Jaunar 2022!

Wir hoffen, dass du bisher einen guten Übergang in das neue Jahr hattest und dich auf die neue Ausgabe freust! Wir denken, dass diese Ausgabe über die Neujahrsfeiertage einen gewissen Umfang angenommen hat.

Viele Projekte und deren Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis von Freiwilligenarbeit. Wenn du mit den von dir verwendeten Diensten und Applikationen zufrieden bist - gerade in der jetzigen Situation - dann denke bitte darüber nach, dich bei diesen Projekten zu bedanken oder helfend einzubringen!

Ließ diesen Newsletter über unseren RSS-Feed:
- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)!
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Bist du daran interessiert, das Newsletter-Team zu unterstützen? Ließ mehr am Ende des Artikels.

Abseits davon - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Die Übersetzungen des XMPP Newsletters werden auf den folgenden Seiten veröffentlicht (mit etwas Verzögerung):

- Die französische Übersetzung erscheint auf [jabberfr.org](https://news.jabberfr.org/category/newsletter/) und [linuxfr.org](https://linuxfr.org/tags/xmpp/public)
- Die deutsche Übersetzung erscheint auf [anoxinon.de](https://anoxinon.de/blog/)
- Die italienische Übersetzung erscheint auf [NicFab.it](https://www.nicfab.it/)
- Die spanische Übersetzung erscheint äquivalent zum englischen Original, wähle einfach [Spanisch in der oberen Leiste des Originalartikels](https://xmpp.org/2022/12/ES/newsletter-dec21-jan22/)

Vielen Dank an die Übersetzenden und deren Arbeit! Das leistet eine klasse Hilfe dabei, die Neuigkeiten verfügbar zu machen! Bitte schließe dich deren Arbeit an oder starte neu mit einer weiteren Sprache!

## XSF Ankündigungen

- Die XSF plant, am [Google Summer of Code 2022 (GSoC) [EN]](https://xmpp.org/community/gsoc-2022/) teilzunehmen. Wenn du ein einer Teilnahme als Student:in interessiert bist oder auch als Mentor:in oder eigenständiges Projekt, dann [ergänze deine Ideen und nimmt mit uns Kontakt auf [EN]](https://wiki.xmpp.org/web/Google_Summer_of_Code_2022)!

![XSF und Google Summer of Code 2022](/img/blog/xmpp_newsletter_dezember_2021_januar_2022/GSoC_2022_Logo.png "XSF und Google Summer of Code 2022")

- Der Blog und die Newsletter-Seiten auf [xmpp.org/blog](https://xmpp.org/blog) unterstützen nun mehrere Sprachen. Wir freuen uns über Freiwillige, die bei der Übersetzung unterstützen!

## XSF fiscal hosting Projekte

The XSF bietet nun [fiscal hosting [EN]](https://xmpp.org/community/fiscalhost/) für XMPP Projekte! Bitte bewirb dich via [Open Collective [EN]](https://opencollective.com/xmpp). Für weitere Informationen siehe auch im [Blogartikel der Ankündigung [EN]](https://xmpp.org/2021/09/the-xsf-as-a-fiscal-host/).

- [Bifrost bridge: Offline Nachrichtenaustausch zwischen Matrix und XMPP [EN]](https://opencollective.com/bifrost-mam)
- [Mellium Co-op [EN]](https://opencollective.com/mellium)

## Veranstaltungen

[XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Schau' doch auch auf unserem neuen [YouTube Kanal [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbei!

[Berlin XMPP Meetup (virtuell)](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Das monatliche Treffen von XMPP-Enthusiasten:innen in Berlin. Jeden zweiten Mittwoch eines jeden Monats.

## Videos

Thilo Molitor (Entwickler von Monal) [hat einen Vortrag](https://xmpp-meetup.in-berlin.de/talks/monal.mp4) über die Entwicklung von Monal gehalten.

XMPP Office Hours: Fabian Sauter hat seine [Abenteuer bei der Entwicklung eines XMPP-Clients [EN]](https://www.youtube.com/watch?v=Bceu6E7c7GM) für Windows (Universal Windows Platform (UWP)) im Dezember präsentiert.

XMPP wurde in einer [deutschen Fernsehsendung](https://youtu.be/DdIwus87eLM?t=844) im Kontext des Datenschutzes erwähnt.

## Artikel

JMP.chat hat zwei Blogbeiträge veröffentlicht. [Die ersten [EN]](https://blog.jmp.chat/b/2022-jabber-xmpp-from-sms) Details einer Funktion des Cheogram Systems vom Soprani.ca Projekt, welches es SMS-Nutzenden erlaubt, jede XMPP-Adresse zu kontaktieren oder anzurufen. [Deren Newsletter](https://blog.jmp.chat/b/january-newsletter-2022) kündigt ebenfalls eine Partnerschaft mit Snikket für Hosting, genauso wie eine Vorschau für weltweite Telefonieverträge an. Sie planen ebendies in Zukunft anzubieten.

![JMP.chat](/img/blog/xmpp_newsletter_dezember_2021_januar_2022/smstoxmpp.png "JMP.chat")

Es gibt einie Reihe von Artikel Rund um das Thema "Messenger" auf der deutschen Webseite "[Freie Messenger](https://www.freie-messenger.de/#neuigkeiten-letzte-änderungen)" mit einem Fokus auf WhatsApp-Alternativen, Ende-zu-Ende-Verschlüsselung, Interoperabilität und Sicherheit/Scheinsicherheit. Hilfe beim Übersetzen der Artikel in deine Muttersprache ist willkommen.

[Endlich wurde OMEMO in Movim integriert [EN]](https://mov.im/?post/pubsub.movim.eu/Movim/end-to-end-encryption-in-movim-omemo-is-finally-there-yudZPP), nach 6 Jahren Diskussionen. In diesem Artikel beschreibt der Movim-Entwickeler Timothée die generelle Architektur von OMEMO, die Schwierigkeiten die bei der Integration in Movim aufgetreten sind, sowie wie diese gelößt werden konnten.

![Movim mit OMEMO-Verschlüsselung [EN]](/img/blog/xmpp_newsletter_dezember_2021_januar_2022/movim_omemo.png "Movim with OMEMO encryption")

Da die [zuvor angekündigte [EN]](https://snikket.org/blog/simply-secure-collaboration/) Zusammenarbeit zwischen Snikket und Simply Secure das erste Projekt abgeschlossen hat, haben sie den Projektgründer, Matthew Wild, über Snikkets Wurzeln und seine Erfahrung in der Verwaltung von quelloffenen Projekten befragt. Lies das Interview: [On Getting Things Done: A Conversation with Matthew Wild from Snikket [EN]](https://simplysecure.org/blog/on-getting-things-done-a-conversation-with-matthew-wild-from-snikket/).

Die Mellium Co-Op veröffentlichte ihren [Jahresrückblick für 2021 [EN]](https://opencollective.com/mellium/updates/mellium-year-in-review-2021) und das [Dev Communiqué für den Dezember 2021 [EN]](https://opencollective.com/mellium/updates/dev-communique-for-december-2021) und [Januar 2022 [EN]](https://opencollective.com/mellium/updates/dev-communique-for-january-2022).

MongooseIM schreibt über [dynamische XMPP Domains in deren Lösung [EN].](https://www.erlang-solutions.com/blog/dynamic-xmpp-domains-in-mongooseim/)

Andrew Lewman prüfte verschiedene Chatprotokolle in einem überlasteten Netzwerk und machte eine [Entdeckung über die Leistungsfähigkeit von XMPP [EN]](https://blog.lewman.com/internet-messaging-versus-congested-network.html) unter solchen Umständen.

Ravi Dwivedi demonstriert in ihrer kurzen Einführung des [Quicksy Android Clients [EN]](https://ravidwivedi.in/posts/quicksy-app/index.html), dass "Freiheit und Datenschutz auch bequem sein können".

Das deutsche Linux-Magazin [testete freie Instant Messenger [EN]](https://www.linux-magazin.de/ausgaben/2022/02/bitparade/) für Linux in der letzten Ausgabe und sah sich, neben anderen, auch den [Gajim Desktop Client [EN]](https://gajim.org/) an.

Eine Analyse über die Gefahr von fehlkonfigurierten XMPP-Servern findet sich in diesem Stück über [XMPP Server Security [EN]](https://bishopfox.com/blog/xmpp-underappreciated-attack-surface) von Bishop Fox.

vanitasvitae veröffentlichte einen [Artikel [EN]](https://blog.jabberhead.tk/2021/12/30/pgpainless-1-0-0-released/), der die Version `1.0.0` von [PGPainless](https://pgpainless.org/) feiert. PGPainless ist eine Java-Bibliothek, die darauf abzielt, die Verwendung von OpenPGP so einfach wie möglich zu machen. Das Projekt wurde im Jahr 2018 als Nebenprodukt eines Google Summer of Code Projekts der XMPP Standards Foundation begonnen!

## Software News

### Clients und Applikationen

[Gajim Neuigkeiten aus der Entwicklung](https://gajim.org/de/post/2022-01-22-development-news-december-january/): Die Arbeit an Gajim Version`1.4` schreitet in großen Schritten voran! Nach neun Monaten Entwicklung des neues Gajim-Hauptfensters war der Quelltext endlich fertig um in den Master-Branch zusammengeführt zu werden. Dies ermöglicht das automatische Erstellen von nightly-Versionen für Linux und Windows.

[monocles chat](https://f-droid.org/de/packages/de.monocles.chat/) (eine Abspaltung von Conversations und Blabber.im) wird in der nächsten Version OTR unterstützen. Der Client erlaubt ausschließlich Verbindungen zu XMPP Servern mit einer aktuellen SSL-Konfiguration und bietet keine Rückfall-SSL-Verbindungen, um Datenlecks zu vermeiden. Nichtsdestrotrotz ist er mit jedem aktuellen XMPP-Konto kompatibel.

[Libervia `0.8` "La Cecília" [EN]](https://www.goffi.org/b/libervia-v0-8-la-cecilia-BdQ4) (bisher bekannt als "Salut à Toi") wurde mit abgeschlossener OMEMO-Verschlüsselung für Gruppenchats veröffentlicht, sowie mit einem neuen Standard-Aussehen, einem einfach zu verwendenden Einlandungssystem, einer nicht-standardbasierten (XMPP) Listenfunktion, mit Fotoalben und vielen technischen Änderungen.

Eine neue stabile Version von [SiskinIM `7.0.1` wurde veröffentlicht [EN]](https://github.com/tigase/siskin-im/releases/tag/7.0.1), welche das Senden von unverschlüsselten Nachrichten in Einzelkonversationen unterstüzt, in denen OMEMO standardmäßig aktiv ist, und bietet darüberhinaus eine automatische Größenbegrenzung für Dateidownloads.

## Server

[Openfire `4.7.0` [EN]](https://discourse.igniterealtime.org/t/openfire-4-7-0-has-been-released/91268) wurde veröffentlicht (zuvor wurde die [Beta veröffentlicht [EN]](https://discourse.igniterealtime.org/t/openfire-4-7-0-beta-hazelcast-plugin-2-6-0-releases/91087)). Diese Veröffentlichug ist die erste nicht-Patch Version nach mehr als einem Jahr, die eine gesunde Anzahl an neuen Funktionen sowie Fehlerbehebungen bringt. Die Höhepunkte dieser Version beinhalten verbesserten Clustering-Support, speziell rund um Gruppenchats, was positiv in großen Umgebungen zum Tragen kommen sollte. Zuvor wurden ebenso [Openfire `4.5.5` [EN]](https://discourse.igniterealtime.org/t/openfire-4-6-6-and-4-5-5-releases-log4j-only-changes/91139), [Openfire `4.6.5` [EN]](https://discourse.igniterealtime.org/t/openfire-4-6-5-released/91108) und [Openfire `4.6.6` [EN]](https://discourse.igniterealtime.org/t/openfire-4-6-6-and-4-5-5-releases-log4j-only-changes/91139) veröffentlicht.

[Prosody `0.11.13` [EN]](https://blog.prosody.im/prosody-0.11.13-released/) wurde veröffentlicht. Seit Dezember hat eine neue Prosody-Version einige Korrekturen sowohl für PEP, um den Speicherverbrauch zu reduzieren ausgeliefert, als auch eine Sicherheitskorrektur für eine Denial of Service Lücke in mod_websocket von Prosody, sowie eine Korrektur für ein Speicherleck. Zuvor wurde [Prosody `0.11.11` [EN]](https://blog.prosody.im/prosody-0.11.11-released/) und [Prosody `0.11.12` [EN]](https://blog.prosody.im/prosody-0.11.12-released/) veröffentlicht.

[ejabberd `21.12` [EN]](https://www.process-one.net/blog/ejabberd-21-12/) wurde veröffentlicht. Die neue ejabberd Version `21.12` wurde nach fünf Monaten Arbeit freigegeben und beinhaltet mehr als einhundert Änderungen, viele davon bedeutende Verbesserungen oder Funktionen, sowie einige Fehlerkorrekturen: PubSub Verbesserungen, ein neues mod_coversejs und Unterstützung für MUC Hats ([XEP-0317 [EN]](https://xmpp.org/extensions/xep-0317.html)).

[Jackal [EN]](https://github.com/ortuman/jackal), ein XMPP-Server geschrieben in Go, hatte [Version `0.56.0` [EN]](https://github.com/ortuman/jackal/releases) veröffentlicht.

Snikket hat deren [Server-Version vom Januar 2022 [EN]](https://snikket.org/blog/jan-2022-server-release/) veröffentlicht, welche ein [Sicherheitslücke schließt](https://snikket.org/blog/snikket-jan-2021-security-release/), das Anfang Januar angekündigt wurde. Die primäre neue Funktion in dieser Version ist die Funktionalität zum Importieren/Exportieren von Konten, der finale Teil des [XMPP account portability Projekts [EN]](https://docs.modernxmpp.org/projects/portability/) das von NGI DAPSI finanziert wird.

![XMPP account portability Projekt [EN]](/img/blog/xmpp_newsletter_dezember_2021_januar_2022/snikket-account-import-export.png "XMPP account portability Projekt")

## Bibliotheken

Eine [neue XMPP-Komponente [EN]](https://gitlab.com/navlost.eu/xmpp/components/webhooks) wurde bekanntgegeben und könnte etwas an Feedback gebrauchen. Die Komponente implementiert [Webhook [EN]](https://en.wikipedia.org/wiki/Webhook) Transporte, die Nutzende (die Person, die die Komponente betreibt oder jede:n andere:n denen sie die Erlaubnis erteilen) HTTP-Endpunkte zum Empfang von Events erstellen lassen und diese in XMPP-Nachrichten übersetzt. Webhook Nutzlasten werden von Middleware verarbeitet und die XMPP-Benachrichtigungen sind vorlagenbasiert im [EJS [EN]](https://ejs.co/ "Embedded JavaScript templating") geschrieben. Aktuell kommt es mit GitLab und reiner Git-Integration, genauso wie einer rohen und ungetesteten Slack Middleware, versteht aber genauso einfache Text- und PNG-, JPEG- und PDF-Inhalte, die Abonnenten als Anhänge via [HTTP File Upload (XEP-0363) [EN]](https://xmpp.org/extensions/xep-0363.html) gesendet werden. Das [Hauptrepositorium findest du hier [EN]](https://gitlab.com/navlost.eu/xmpp/components/webhooks) (bisher nicht in produktivbereiter Qualität) und es gibt ebenfalls einen [Demo Server [EN]](https://gitlab.com/navlost.eu/xmpp/components/webhooks/-/blob/devel/README.md#demo-server) der für gelegentliches Testen zur Verfügung steht.


## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Lies mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

- [Compatibility Fallback [EN]](https://xmpp.org/extensions/inbox/compatibility-fallback.html)
    - Dieses Dokument definiert eine Möglichkeit, um anzuzeigen, dass ein bestimmter Teil des Körpers nur als Fallback dient und für welche Spezifikation der Fallback gilt.
- [Call invites[EN]](https://xmpp.org/extensions/inbox/call-invites.html)
    - Dieses Dokument definiert, wie man jemanden zu einem Anruf einlädt und wie man auf die Einladung antwortet.
- [PubSub Namespaces [EN]](https://xmpp.org/extensions/inbox/pubsub-ns.html)
    - Diese Erweiterung definiert ein neues PubSub-Knotenattribut zur Angabe des Typs der Nutzlast.
- [Message Replies [EN]](https://xmpp.org/extensions/inbox/replies.html)
    - Dieses Dokument definiert einen Weg, um anzuzeigen, dass eine Nachricht eine Antwort auf eine vorherige Nachricht ist.

### New (Neu)

* Diesen Monat gibt es keine neuen XEPs.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

* Diesen Monat wurden keine XEPs zurückgestellt.

### Updated (Aktualisiert)

- Version `1.1.0` von [XEP-0363 [EN]](https://xmpp.org/extensions/xep-0363.html) (HTTP-File-Upload)
    - Größe des Dateinamens in Bytes.
    - Header MÜSSEN in der PUT-Anfrage enthalten sein.
    - Header werden als undurchsichtig angesehen.
    - Server können aus Sicherheitsgründen Header signieren wollen.
    - Erlaubt Groß-/Kleinschreibung der Header, mehrere Male denselben Header, und behält die Reihenfolge in der HTTP-Anfrage bei. (egp, mb)
- Version `0.4.0` von [XEP-0353 [EN]](https://xmpp.org/extensions/xep-0353.html) (Jingle Message Initiation)
    - Überarbeitung der gesamten Spezifikation, Namespace Bump
    - Hinzufügen einer neuen Nachricht
    - Hinzufügen der Abhängigkeit von XEP-0280, XEP-0313 und XEP-0334
    - Hinzufügen zu einigen Nachrichten (tm)
- Version `1.1.0` von [XEP-0459 [EN]](https://xmpp.org/extensions/xep-0459.html) (XMPP Compliance Suites 2022)
    - Ersetzen von veraltetem XEP-0411 durch XEP-0402 im erweiterten Gruppenchat (egp)
- Version `0.4.0` von [XEP-0380 EN](https://xmpp.org/extensions/xep-0380.html) (Explicit Message Encryption)
    - Hinzufügen neuer OMEMO-Namensräume: 'urn:xmpp:omemo:1' für OMEMO-Versionen ab `0.4.0`, und 'urn:xmpp:omemo:2' für OMEMO-Versionen ab `0.8.0` (melvo)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

* Letzter Aufruf zur Einreichung von Kommentaren zu [XEP-0424 [EN]](https://xmpp.org/extensions/xep-0424.html) (Message Retraction)
* Letzter Aufruf zur Einreichung von Kommentaren zu [XEP-0425 [EN]](https://xmpp.org/extensions/xep-0425.html) (Message Moderation)

### Stable (Stabil)
Info: Die XSF hat beschlossen, "Draft" in "Stable" umzubenennen. [Lies mehr darüber hier [EN]](https://github.com/xsf/xeps/pull/1100).

* In diesem Monat wurden keine XEPs in die Kategorie "Stable" eingestuft.

### Deprecated (Veraltet)

* [XEP-0256 [EN]](https://xmpp.org/extensions/xep-0256.html) (Last Activity in Presence)

### Obsoleted (Obsolet)

* [XEP-0443 [EN]](https://xmpp.org/extensions/xep-0443.html) (XMPP Compliance Suites 2021)

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

* Kein Call for Experience in diesem Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an Adrien Bourmault (neox), Anoxinon e.V., arne, emus, Goffi, IM, Licaon_Kter, MattJ, mdosch, NicFab, Sam Whited, TheCoffeMaker, vanitasvitae, wurstsalat3000 ihre Hilfe, ihn zu erstellen!

Vielen Dank an alle Ünterstützenden und ihre Hilfe!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen. Wir brauchen wirklich mehr Unterstützung!

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch, Französisch, Italienisch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

