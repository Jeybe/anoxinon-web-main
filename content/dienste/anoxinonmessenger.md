+++
title = "Anoxinon Messenger"
description = "XMPP Server"
draft = false
weight = 3
bref="XMPP Server aus Deutschland"
toc = true
+++

### Was ist Anoxinon Messenger?

Anoxinon Messenger ist der Instant Messenger von Anoxinon. Er basiert auf XMPP, einem anerkannten und offenen Standard für Messaging. Man kann ähnlich wie bei WhatsApp und Konsorten Nachrichten mit beliebigen Kontakten austauschen, Gruppenchats erstellen, Sprachnachrichten, Bilder, Videos und beliebige andere Dateien versenden. Auf Wunsch ist das auch Ende zu Ende verschlüsselt möglich. Anoxinon Messenger kennzeichnet sich durch folgende Merkmale:

* Nachrichtenaustausch mit beliebigen Kontakten
* Gruppenchats mit bis zu 500 Kontakten
* Kein Tracking und keine Analyse von Daten zu Werbezwecken oder der kommerziellen Profilbildung
* Dezentral & Föderal
* Interessante öffentliche Gruppenchats
* Reichweite von mehreren Tausend Nutzer:innen

<a class="btn-mini" href="#registrieren">Zur Registrierung</a> | <a class="btn-mini" href="https://webchat.anoxinon.me/">Zum Webchat</a>

Bitte beachtet unsere [Nutzungsbedingungen](https://anoxinon.de/nutzungsbedingungen_xmpp/) !

### Was ist XMPP?

[XMPP](https://xmpp.org) ist ein offener Standard, der zum Beispiel zum Austausch von Chat-Nachrichten verwendet werden kann. Das bedeutet, XMPP ist praktisch eine *Bauanleitung* für einen Messenger - jede Person mit entsprechenden Fähigkeiten kann sich mit XMPP einen eigenen Messenger bauen. Das bedeutet, dass es viele verschiedene XMPP-Anbieter:innen und XMPP-Apps gibt. Und das Beste: Weil alle auf der gleichen Bauanleitung - XMPP - basieren, sind sie grundsätzlich kompatibel miteinander. Das bedeutet konkret, dass du dir als Nutzer:in von Anoxinon Messenger aussuchen kannst, welche Messaging-App du verwenden möchtest und - noch besser - auch mit Nutzer:innen bei anderen XMPP-Anbieter:innen chatten kannst.


Mehr Informationen und häufige Fragen finden sich in unserem [**FAQ-Bereich**](/faq/).

#### Welche Messaging-Apps gibt es?

##### Android

* **Conversations:** Conversations ist eine moderne und zuverlässige Messaging-App für Android, die alle relevanten XMPP-Funktionen unterstützt. Selbstverständlich ist Conversations freie Software und kommt ohne Tracking daher. Man kann Conversations aus [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations) oder aus dem [Play Store](https://play.google.com/store/apps/details?id=eu.siacs.conversations) beziehen. <br>
**Hinweis:** Messtome hat eine Anleitung zur Benutzung von Conversations erstellt, die unter der [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) frei verwendet und weitergeben werden darf. Für Interessierte bieten wir die Anleitung [**zum Download**](https://anoxinon.media/files/Anleitung_Conversations.pdf) an.
* **blabber.im:** blabber.im ist eine Abspaltung von Conversations und zielt vor allem darauf ab, ein an herkömmliche Messenger (WhatsApp etc...) angelehntes, einfaches Bedienkonzept bereitzustellen. Auch blabber.im unterstützt alle relevanten Funktionen und ist selbstverständlich freie Software, die euch nicht trackt. Man kann blabber.im aus [F-Droid](https://f-droid.org/en/packages/de.pixart.messenger/) oder dem [Play-Store](https://play.google.com/store/apps/details?id=im.blabber.messenger) beziehen.

#### iOS

**Hinweis:** Die Messaging-Apps für XMPP auf iOS sind generell noch nicht so ausgereift wie die auf Android. Das betrifft zum einen die Funktionen und zum anderen die Fehlerfreiheit. Wenn ihr Anoxinon Messenger auf iOS verwenden wollt, bringt am besten etwas Geduld mit und probiert, welche der Apps für euch am besten funktioniert.

* **Siskin-IM:** Siskin-IM ist eine Messaging App für Apple's iPhones. Grundfunktionen wie der verschlüsselte Chat mit Kontakten und Gruppenchats werden unterstützt, letztere allerdings nur ohne Ende zu Ende Verschlüsselung. Beachten muss man außerdem, dass man nach der Installation die Einstellungen durchklicken sollte, da manche nützliche Funktionen von Haus aus deaktiviert sind. Auch Siskin-IM ist freie Software und trackt euch nicht. Man kann Siskin-IM aus [Apple's App Store](https://apps.apple.com/de/app/tigase-messenger/id1153516838) beziehen.
* **Monal:** Monal ist eine weitere Messaging App für iOS. Grundfunktionen  wie der verschlüsselte Chat mit Kontakten und Gruppenchats werden unterstützt, letztere allerdings nur ohne Ende zu Ende Verschlüsselung. Auch Monal ist freie Software und trackt euch nicht. Man kann Monal aus [Apple's App Store](https://itunes.apple.com/us/app/monal-free-xmpp-chat/id317711500?mt=8) beziehen.

##### Web

* **Anoxinon ConverseJS:** XMPP kann auch vom PC aus problemlos via Browser verwendet werden. Anoxinon bietet dazu einen eigenen Webchat auf Basis des freien und trackingfreien [ConverseJS](https://conversejs.org) an. Zu [Anxoinon ConverseJS](https://webchat.anoxinon.me/).

<h3 id="registrieren">Registrieren</h3>

<iframe src="https://xmpp.anoxinon.me/register/new/" class="xmpp-signup-form" frameBorder="0"></iframe>
